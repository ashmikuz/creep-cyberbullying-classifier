# Configuration files

The configuration files are in the json file format (see default.json for an example) and are used to specify which hyperparameters, features and dataset should be tested. The system will train all possible combinations of parameters selected and report results (on the test set) obtained from the best one, according to performance on the validation set. 

## Features

The system supports two variables related to features, ``text_features`` and ``other_features``.
The ```all_text_features``` are:

* ``word2vec``: word embeddings;
* ``unigrams``, ``bigrams``: unigrams and bigrams of lemmas;
* ``pos_unigrams``, ``pos_bigrams``: Part Of Speech unigrams and bigrams;
* ``cbows``: sum of all the word embeddings;
* ``char-rnn``: a recurrent neural netword trained on sequences of characters.

The ```all_other_features``` are:

* ``emolex``, ``hurtlex``: emotion and hutful words lexica;
* ``sentiment``: sentiment polarity;
* ``social``: social specific features (number of hashtags, "@" mentions, exclamation marks, question marks, uppercase words);
* ``img_creender_predictions``, ``img_creender_descriptors``, ``img_sentiment_predictions``, ``img_sentiment_descriptors``: features derived from images (only for the "instagram-cyberbullying" dataset)

## Models

The variable ``all_model_types`` specifies the type of classifier desired. Three variables are supported:

* ``nnet``: a neural network;
* ``svm``: a support vector machine classifier;
* ``lr``: a logistic regression classifier.

## Hyperparameters

Some variables are used to specify hyperparameters. These are:

* ``all_labmdas``: all constants for L2 regularization;
* ``all_batch_sizes``: all batch sizes used to train the model;
* ``all_hidden_layer_sizes``: the size and number of hidden layers (only for the ``nnet`` model);
* ``all_dropouts``: all dropouts to be applied to the output of the feed forward layers (only for the ``nnet`` model);
* ``all_batch_normalization``: whether to apply batch normalization to the output of feed forward layers (only for the ``nnet`` model.

## RNN specific Hyperparameters

Some variables can only be used when the ``word2vec`` feature is activated:

* ``all_recurrent_layer_sizes``: all number and sizes of recurrent layers;
* ``all_recurrent_dropouts``: all recurrent dropouts (applied to the recurrent layer);
* ``all_word2vec_sources``: specify the embeddings source. See the embeddings readme for the possible values;
* ``all_recurrent_layer_types``: type of recurrent layer(s) to use. ``BiLSTM``, ``GRU`` and ``LSTM`` are supported;
* ``all_attention``: whether to apply a simple attention mechanism to the outputs of the recurrent layer.

## Other parameters

Finally, we have some miscellaneous parameters:

* ``train_dataset``,``test_dataset``: specify the train and test dataset (see the data readme for possible values);
* ``all_emoji``: specify how to treat emoji. Possible values are ``vec_emoji``, that use emoji embeddings; ``false``, that ignores emojis (for embeddings); ``transcribe_emojis`` that transcribes them to strings.

##Example

We provide an example of a configuration that will:

* use both an svm and a neural network;
* train and test on the hatespeech-waseem dataset, using binary labels;
* use either no L2 regularization or a constant of value 0.1;
* use batch size 32;
* ignoring emojis;
* using only unigrams, bigrams as features for the SVM, either only embeddings or unigrams and bigrams for the neural network;
* using either only social features or no additional feature;

The neural network will have:

* either one hidden layer of size 200 or two hidden layers of size 100;
* either no dropout or dropout of 0.5
* either no batch normalization or batch normalization applied to FF layer(s).

If it uses the embeddings: 

* either fasttext-crawl or google (word2vec) embeddings;
* either a GRU or a LSTM as recurrent layer;
* either one recurrent layer of size 100 or two of size 50;
* no attention or simple attention.

```json
{
	"all_layer_sizes": [[200],[100,100]], 
	"all_recurrent_layer_sizes": [[100], [50,50]], 
	"all_lambdas": [0, 0.1], 
	"all_batch_sizes": [32], 
	"all_dropouts": [0,0.5], 
	"all_recurrent_dropouts": [0, 0.2], 
	"all_use_batch_normalization": [false,true], 
	"all_model_types": ["nnet","svm"], 
	"train_dataset": "hatespeech-waseem-binary", 
	"test_dataset": "hatespeech-waseem-binary", 
	"all_word2vec_sources": ["fasttext-crawl","google"], 
	"all_text_features": [["word2vec"], ["unigrams", "bigrams"]], 
	"all_other_features": [["social"], []], 
	"all_emojis": [false], 
	"all_recurrent_layer_types": ["GRU","LSTM"], 
	"all_attentions": [false, true]
}

```
