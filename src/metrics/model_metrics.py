#!/usr/bin/env python
from tensorflow.python.keras import backend as K

def precision(y_true, y_pred):
    true_round=K.round(y_true)
    pred_round=K.round(y_pred)
    true_positives=K.sum(true_round * pred_round)
    predicted_positives=K.sum(pred_round)
    precision=true_positives/(predicted_positives+K.epsilon())
    return precision

def recall(y_true,y_pred):
    true_round=K.round(y_true)
    pred_round=K.round(y_pred)
    true_positives=K.sum(true_round * pred_round)
    correct_positives=K.sum(true_round)
    recall=true_positives/(correct_positives+K.epsilon())
    return recall

def fscore(y_true, y_pred):
    precision=precision(y_true,y_pred)
    recall=recall(y_true,y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))

