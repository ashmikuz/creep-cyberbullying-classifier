#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import itertools
from sklearn.metrics import confusion_matrix

def plot_confusion_matrix(cm, classes,
                          dataset_name,
                          normalize=True,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues,
                          ):
    """
    This function plots the confusion matrix to file.

    Args:
        cm: confusion matrix (from sklearn)
        classes: dataset classes
        dataset_name: dataset name
        normalize: normalize the matrix (default=True)
        title: matrix title (default="Confusion matrix")
        cmap: color map
    
    """
    import time

    if normalize:
        cm_sum = np.maximum(cm.sum(axis=1)[:, np.newaxis], 1)
        cm = cm.astype('float') / cm_sum
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')


    plt.figure()
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

    time_str=time.strftime("%Y%m%d%H%M",time.gmtime())
    plt.savefig('../plots/confusion_matrix_{}_{}'.format(dataset_name,time_str))

    return


def plot_results(result_matrices, training_epochs, fields, dataset_name):
    """Plots the validation set results for each epoch in a file

    Args:
        result_matricses: result matrices (indexed by epoch)
        training_epochs: number of training epochs
        fields: classes in the dataset
        dataset_name: dataset name
    """

    import time
    import math

    legend=['accuracy','recall','fscore']
    
    #FIXME 
    if(len(fields)==2):
        f, subplot_list = plt.subplots(2,2)
    else:
        num_plots=math.ceil(len(fields)+1)+2
        num_rows=math.ceil(num_plots/2.)
        f, subplot_list = plt.subplots(num_rows,2)

    f.suptitle("Validation set results")

    subplot_list=subplot_list.flatten()
    
    range_x=range(1, training_epochs+1)
    for category in range(result_matrices.shape[1]):
        curr_plot=subplot_list[category]
        for metric_idx in range(3):
            curr_plot.plot(range_x, result_matrices[:,category,metric_idx],
                     label=legend[metric_idx])
        if(category<len(fields)):
            curr_plot.set_title(fields[category])
        elif(category==len(fields)+1):
            curr_plot.set_title("macro_avg")
        else:
            curr_plot.set_title("micro_avg")
        curr_plot.legend()
    
    time_str=time.strftime("%Y%m%d%H%M",time.gmtime())
    plt.savefig('../plots/validation_{}_{}'.format(dataset_name,time_str))

    return

def get_performance_metrics(fields, results, model_conf, y_test): 
    """ Get performance metrics and confusion matrix
    
    Args:
        fields: classes in the dataset
        results: predictions from the model
        model_conf: ModelConfiguration object
        y_test: true labels

    Returns:
        metrics matrix (precision,recall,fscore over the provided labels)
        and confusion matrix
    """
    from sklearn.metrics import precision_recall_fscore_support

    if(len(fields)>2):
        results_categorical = np.argmax(results, axis=1)
        labels_categorical = np.argmax(y_test, axis=1)
    elif(model_conf.model_type=='svm'):
        results_categorical=np.where(results>0, 1, 0)
        labels_categorical = y_test 
    else:
        results_categorical = np.round(results)
        labels_categorical = y_test

    # DEBUG fix the size so we can test with 1-sized sample sets
    # labels_categorical = labels_categorical[:len(results_categorical)]
    
    metrics_matrix = np.zeros((len(fields)+2, 4))

    # get precision, recall and f-score for each category 
    tp_total=0.
    fp_total=0.
    fn_total=0.
    for field_idx in range(len(fields)+1):
        
        results_field = (results_categorical == field_idx)
        y_curr_category = (labels_categorical == field_idx)

        tp = (np.sum(results_field * y_curr_category))
        fp = (np.sum(results_field * (1-y_curr_category)))
        fn = (np.sum((1-results_field) * y_curr_category))

        tp_total+=tp
        fp_total+=fp
        fn_total+=fn
        
        if((tp+fp)==0):
            precision=0.0
        else:
            precision = tp/(tp+fp)
        
        if((tp+fn)==0):
            recall=0
        else:
            recall = tp/(tp+fn)
        
        if((precision+recall)==0):
            fscore=0
        else:
            fscore = 2*(precision*recall)/(precision + recall)
        support = np.sum(y_curr_category)

        metrics_matrix[field_idx, 0] = precision
        metrics_matrix[field_idx, 1] = recall
        metrics_matrix[field_idx, 2] = fscore
        metrics_matrix[field_idx, 3] = support

    
    #macro avg
    metrics_matrix[-2, -1] = np.sum(metrics_matrix[:-1, 3])
    for i in range(3):
        metrics_matrix[-2, i]= (np.sum(metrics_matrix[:-2,i])/len(fields))
    
    if((tp_total+fp_total)==0):
        precision_microavg=0.0
    else:
        precision_microavg=tp_total/(tp_total+fp_total)

    if((tp_total+fn_total)==0):
        recall_microavg=0.0
    else:
        recall_microavg=tp_total/(tp_total+fn_total)

    if(precision_microavg+recall_microavg==0):
        fscore_microavg=0.0
    else:
        fscore_microavg=2*(precision_microavg*recall_microavg)/(precision_microavg+recall_microavg)

    metrics_matrix[-1,-1] = metrics_matrix[-2,-1]
    metrics_matrix[-1,0] = precision_microavg
    metrics_matrix[-1,1] = recall_microavg
    metrics_matrix[-1,2] = fscore_microavg

    if(len(fields)>1):
        conf_matrix = confusion_matrix(labels_categorical, results_categorical,
                                   labels=range(len(fields)))
    else:
        conf_matrix = confusion_matrix(labels_categorical, results_categorical)

    print(metrics_matrix)

    return metrics_matrix, conf_matrix

def print_result_table(matrix, fields):

    for i in range(len(fields)):
        precision = matrix[i][0]
        recall = matrix[i][1]
        fscore = matrix[i][2]
        support = matrix[i][3]

        print('{}: precision {} recall {} fscore {} '
              'support {}'.format(fields[i], precision, recall,
              fscore, support))
    
    precision = matrix[-2, 0]
    recall = matrix[-2, 1]
    fscore = matrix[-2, 2]
    support = matrix[-2, 3]
    print('macro_avg: precision {} recall {} fscore {} '
          ' support {}'.format(precision, recall, fscore, support))

    precision = matrix[-1, 0]
    recall = matrix[-1, 1]
    fscore = matrix[-1, 2]
    support = matrix[-1, 3]
    print('mirco_avg: precision {} recall {} fscore {} '
          ' support {}'.format(precision, recall, fscore, support))

    return
