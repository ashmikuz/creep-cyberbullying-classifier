#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import csv
import json
import re
from preprocess.urlregex import WEB_URL_REGEX
from sklearn.model_selection import train_test_split

def get_field_names(dataset_name):
    """Get the (binary or categorical) label names for a given dataset

        Args:
            dataset_name: the name of the currently used dataset

        Returns:
            the label names for the specific dataset
    """
    fields={'argumentative':['argument'], 'hate-speech':['hate-speech'],
            'dart':['argument'], 
            'hatespeech-waseem-categorical':['racism','sexism','none'],
            'hatespeech-waseem-binary':['non hate-speech','hate-speech'],
            'hatespeech-waseem-binary-reduced':['non hate-speech','hate-speech'],
            'bullyingv3-binary':['non bullying trace','bullying trace'],
            'instagram-cyberbullying':['non cyberbullying','cyberbullying'],
            'twitter-itaonly':['non hate-speech', 'hate-speech'],
            'twitter-itaeng': ['non hate-speech', 'hate-speech'],
            'evalita-twitter': ['non nate-speech', 'hate-speech'],
            'evalita-facebook': ['non nate-speech', 'hate-speech'],
            'evalita-fbtot': ['non hate-speech', 'hate-speech'],
            'evalita-twtofb': ['non hate-speech', 'hate-speech'],
            'evalita-both':['non hate-speech', 'hate-speech'],
            'whatsapp+evalita-twitter' : ['non hate-speech','hate-speech'],
            'germeval2018-binary': ['non offensive', 'offensive'],
            'germeval2018-binary-reduced': ['non offensive', 'offensive'],
            'germeval2018-categorical': ['other','abuse','insult','profanity'],
            'germeval2019-task1': ['non offensive', 'offensive'],
            'germeval2019-task2': ['other','abuse','insult','profanity'],
            'germeval2019-task3': ['explicit', 'implicit'],
            'german-hatespeech' : ['non hate-speech','hate-speech'],
            'hateval-es': ['non hate-speech', 'hate-speech'],
            'misogyny': ['non misogynous','misogynous'],
            'cyberbullying-ita' : ['non hate-speech', 'hate-speech'],
            'instagram-eng-testonly': ['non hate-speech','hate-speech'],
            'instagram-ita2': ['non hate-speech', 'hate-speech'],
            'twitter+facebook+instagram+whatsapp': ['non hate-speech', 'hate-speech'],
            'twitter+facebook+instagram+whatsapp<280': ['non hate-speech', 'hate-speech'],
            'whatsapp' : ['non hate-speech', 'hate-speech']
            }
    return fields[dataset_name]


def get_data(train_category, test_category):
    train_val_data=list(get_data_single_category(train_category)[:-1])
    test_category=[get_data_single_category(test_category)[-1]]
    return train_val_data+test_category

def get_data_single_category(category):
    """Read data and split it 60/40 for training/testing
        
        Args:   
            category: the dataset to import
        
        Returns:
            a tuple containing training,test data
    """
    if(category=='argumentative'):
        train_data=get_data_argumentative("meta/",['grexit'])
        test_data=get_data_argumentative("meta/",['brexit'])
    elif(category=='hate-speech'):
        train_data,test_data=get_data_hatespeech()
    elif(category=='dart'):
        train_data=get_data_argumentative('../datas/dart/',
                                          ['47traitors','got','grexit'], 
                                          sentiment_data_source='alchemy')
        test_data=get_data_argumentative('../datas/dart/', 
                                         ['iwatch'], 
                                         sentiment_data_source='alchemy')
    elif(category=='hatespeech-waseem-categorical'):
        return get_data_waseem(True)
    elif(category=='hatespeech-waseem-binary'):
        return get_data_waseem(False)
    elif(category=='hatespeech-waseem-binary-reduced'):
        return get_data_waseem_reduced()
    elif(category=='bullyingv3-binary'):
        train_data,test_data = get_data_bullying()
    elif(category=='instagram-cyberbullying'):
        train_data,test_data = get_data_instagram()
    elif(category=='twitter-itaonly'):
        train_data, test_data = get_data_twitter_ita()
    elif(category=='twitter-itaeng'):
        return get_data_twitter_itaeng()
    elif(category=='evalita-facebook'):
        return get_data_evalita_facebook()
    elif(category=='evalita-twitter'):
        return get_data_evalita_twitter()
    elif(category=='evalita-twtofb'):
        return get_data_evalitamixed(True, False)
    elif(category=='evalita-fbtot'):
        return get_data_evalitamixed(False, True)
    elif(category=='evalita-both'):
        return get_data_evalitamixed(False, False)
    elif(category=='whatsapp+evalita-twitter'):
        return get_data_cyberbullying_italian(False)
    elif(category=='germeval2018-binary'):
        return get_data_germeval2018(False)
    elif(category=='germeval2018-categorical'):
        return get_data_germeval2018(True)
    elif(category=='germeval2018-binary-reduced'):
        return get_data_germeval2018_reduced()
    elif(category=='germeval2019-task1'):
        return get_data_germeval2019_task12(False)
    elif(category=='germeval2019-task2'):
        return get_data_germeval2019_task12(True)
    elif(category=='germeval2019-task3'):
        return get_data_germeval2019_task3()
    elif(category=='german-hatespeech'):
        train_data, test_data = get_data_german_hatespeech()
    elif(category=='hateval-es'):
        return get_data_hateval_es()
    elif(category=='misogyny'):
        train_data, test_data = get_data_misogyny(0)
    elif(category=='cyberbullying-ita'):
        return get_data_cyberbullying_italian()
    elif(category=='instagram-eng-testonly'):
        return get_data_cyberbullying_instagram_testonly()
    elif(category=='instagram-ita2'):
        return get_data_instagram_clicit()
    elif(category=='twitter+facebook+instagram+whatsapp'):
        return get_data_all_clicit()
    elif(category=='twitter+facebook+instagram+whatsapp<280'):
        return get_data_all_clicit(True)
    elif(category=='whatsapp'):
        return get_data_whatsapp_ita()
    else:
        raise Exception('dataset {} not found'.format(category))
    return(train_data,test_data)

def get_data_hateval_es():
    tsvfile=open('../data/hateval/public_development_es/train_es.tsv','r')
    reader=csv.DictReader(tsvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    train_data=[]
    for line in reader:
        curr_dict={}
        curr_dict['text']=line['text']
        curr_dict['language']='es'
        curr_dict['hate-speech']=int(line['HS'])
        train_data.append(curr_dict)

    train_data=np.asarray(train_data)

    tsvfile=open('../data/hateval/public_development_es/dev_es.tsv','r')
    reader=csv.DictReader(tsvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    val_data=[]
    for line in reader:
        curr_dict={}
        curr_dict['text']=line['text']
        curr_dict['language']='es'
        curr_dict['hate-speech']=int(line['HS'])
        val_data.append(curr_dict)

    val_data=np.asarray(val_data)

    tsvfile=open('../data/hateval/reference_test_es/es.tsv','r')
    reader=csv.reader(tsvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    test_data=[]
    for line in reader:
        curr_dict={}
        curr_dict['text']=line[1]
        curr_dict['language']='es'
        curr_dict['hate-speech']=int(line[2])
        test_data.append(curr_dict)

    test_data=np.asarray(test_data)

    return train_data, val_data, test_data

def get_data_instagram_clicit():
    tsvfile=open('../data/insta-ita/Ita-Instagram-Ita.tsv','r')
    reader=csv.reader(tsvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    data=[]
    for line in reader:
        curr_dict={}
        curr_dict['text']=line[1]
        curr_dict['language']='it'
        curr_dict['hate-speech']=int(line[2])
        data.append(curr_dict)

    train_data, test_data=train_test_split(data, test_size=0.4, 
                                           random_state=42)

    return train_data, test_data

def get_data_all_clicit(limit_280=False):

    train_data_twitter, val_data_twitter, test_data_twitter = get_data_evalita_twitter()
    train_data_fb, val_data_fb, test_data_fb = get_data_evalita_facebook()

    train_data_wapp, test_data_wapp = get_data_whatsapp_ita()
    validation_input_no = int(round(len(test_data_wapp)*0.5))
    validation_data_wapp = test_data_wapp[:validation_input_no]
    test_data_wapp=test_data_wapp[validation_input_no:]



    train_data_insta, test_data_insta = get_data_instagram_clicit()
    validation_input_no = int(round(len(test_data_insta)*0.5))
    validation_data_insta = test_data_insta[:validation_input_no]
    test_data_insta=test_data_insta[validation_input_no:]
    
    train_data_all=np.concatenate((train_data_twitter, train_data_fb,
                                   train_data_wapp, train_data_insta), axis=0)
    
    if(limit_280):
        train_limited=[]
        for line in train_data_all:
            line_replaced=re.sub(r'(^|\s)@(\w+)', r"\1",line['text'])
            line_replaced=re.sub(WEB_URL_REGEX, "", line_replaced)
            if(len(line_replaced)<=280):
                train_limited.append(line)
        train_data_all=np.asarray(train_limited)

    return train_data_all, None

def get_data_cyberbullying_instagram_testonly():
    tsvfile=open('../data/instagram-eng-testonly/annotazioni_insta_en.tsv','r')
    reader=csv.reader(tsvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    data=[]
    for line in reader:
        curr_dict={}
        curr_dict['text']=line[1]
        curr_dict['language']='en'
        curr_dict['hate-speech']=int(line[2])
        data.append(curr_dict)

    data=np.asarray(data)
    return None, None, data

def get_data_whatsapp_ita():
    import re
    users_re=re.compile(r'[SupportoVittima|SupportoBullo|Vittima|Bullo][^:]*:',re.UNICODE)
    jsonfile = open('../data/cyberbullying-italian/data_whatsapp.json','r')
    whatsapp_json = json.load(jsonfile)
    data=[]
    old_user=None
    curr_msg=None
    for value in whatsapp_json:
        text = " ".join(value['text'])

        split_users=re.split(r'([SupportoVittima|SupportoBullo|Vittima|Bullo][^:]*:)',text, maxsplit=1)
        text = split_users[-1]
        text=re.sub(r'(SupportoVittima|SupportoBullo|Vittima|Bullo)[0-9]*',"username", text)
        if(len(split_users)>1):
            user = split_users[-2]
        
        #append last message only if new user
        if(user != old_user):
            if(curr_msg is not None):
                data.append(curr_msg)
            old_user=user
            curr_msg={}
            cyberbullying=False
            curr_msg['text']=''

        text=re.sub(r'(SupportoVittima|SupportoBullo|Vittima|Bullo)[0-9]*',"username", text)

        curr_msg['text'] += " "+text
        for annotation in value['annotation']:
            if(annotation['entity-type']!='Defense' 
               and annotation['entity-type']!='Encouragement_to_the_Harasser' 
               and not annotation['non-offensive'] and
               annotation['role-type'] != 'Victim'):
                cyberbullying=True
        curr_msg['hate-speech'] = int(cyberbullying)
        curr_msg['language'] = 'it'
    
    #append last message
    data.append(curr_msg)

    data=np.asarray(data)

    jsonfile.close()

    train_data, test_data=train_test_split(data, test_size=0.4, 
                                           random_state=42)
    return train_data, test_data

def get_data_instagramtw_ita():
    data=[]
    jsonfile = open('../data/cyberbullying-italian/data_instatw.json','r')
    instatw_json = json.load(jsonfile)
    curr_user=None
    for value in instatw_json:
        curr_msg={}
        token_list=value['text'][4:]
        for token_idx in range(len(token_list)):
            if('[[[' == token_list[token_idx][:3]):
                token_hexstring=token_list[token_idx][3:8]
                token_bytes=int(token_hexstring, 16)

                token_unicode=chr(token_bytes)
                token_list[token_idx]=token_unicode

        curr_msg['text'] =" ".join(token_list)

        cyberbullying=False
        for annotation in value['annotation']:
            if(annotation['entity-type']!='Defense' 
               and annotation['entity-type']!='Encouragement_to_the_Harasser' 
               and not annotation['non-offensive'] and
               annotation['role-type'] != 'Victim'):
                cyberbullying=True
        curr_msg['hate-speech'] = int(cyberbullying)
        curr_msg['language'] = 'it'
        data.append(curr_msg)
    
    data=np.asarray(data)
    jsonfile.close()
    return data


def get_data_cyberbullying_italian(use_instatw=True):
    data_whatsapp=get_data_whatsapp_ita()
    
    if(use_instatw):
        data_instatw = get_data_instagramtw_ita()
    else:
        data_instatw=np.zeros((0,))

    train_tw,val_tw,test_tw=get_data_evalita_twitter()

    train_data=np.concatenate((data_whatsapp, data_instatw, train_tw), axis=0)


    positive_instances = np.sum([t['hate-speech'] for t in train_data])
    print('train data: {} positive out of {}'.format(positive_instances, len(train_data)))
    positive_instances = np.sum([t['hate-speech'] for t in test_tw])
    print('test data: {} positive out of {}'.format(positive_instances, len(test_tw)))

    return train_data, val_tw, test_tw

def get_data_misogyny(task_num):
    lang_list=['en','it']
    misogyny_categories=['stereotype', 'sexual_harassment', '0', 'discredit', 'derailing', 'dominance']
    data=[]
    for lang in lang_list:
        file_name="../data/evalita18/misogyny/{}_training.tsv".format(lang)
        csvfile=open(file_name,'r')
        reader=csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
        for line  in reader:
            tweet={}
            tweet['text']=line['text']
            tweet['misogynous']=int(line['misogynous'])
            tweet['active']=int(line['target']=='active')
            for category in misogyny_categories:
                tweet[category]= int(line['misogyny_category']==category)

            tweet['language']=lang
            data.append(tweet)
    
    data=np.asarray(data)
    
    train_data, test_data=train_test_split(data, test_size=0.4, 
                                                  random_state=42)
    return train_data, test_data

def get_data_german_hatespeech():
    csvfile=open('../data/german-hatespeech/german_hatespeech_refugees.csv','r')
    reader=csv.DictReader(csvfile)
    data=[]
    for line in reader:
        tweet={}
        tweet['text']=line['Tweet']
        annotator_1=line['HatespeechOrNot (Expert 1)']=='YES'
        annotator_2=line['HatespeechOrNot (Expert 2)']=='YES'
        tweet['hate-speech']=int(annotator_1 and annotator_2)
        tweet['language']='de'
        data.append(tweet)

    data=np.asarray(data)

    train_data, test_data=train_test_split(data, test_size=0.4, 
                                                  random_state=42)
    return train_data, test_data


def get_data_germeval2019_task12(categorical):
    csvfile=open('../data/germeval/germeval2019_training_subtask12.txt','r')
    reader=csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    data=[]
    for line in reader:
        post={}
        post['text']=line[0]
        if(categorical):
            post['abuse']=int(line[2]=='ABUSE')
            post['insult']=int(line[2]=='INSULT')
            post['profanity']=int(line[2]=='PROFANITY')
            post['other']=int(line[2]=='OTHER')
        else:
            post['offensive']=int(line[1]=='OFFENSE')

        post['language']='de'
        data.append(post)

    csvfile=open('../data/germeval/germeval2018.training.txt','r')

    for line in reader:
        post={}
        post['text']=line[0]
        if(categorical):
            post['abuse']=int(line[2]=='ABUSE')
            post['insult']=int(line[2]=='INSULT')
            post['profanity']=int(line[2]=='PROFANITY')
            post['other']=int(line[2]=='OTHER')
        else:
            post['offensive']=int(line[1]=='OFFENSE')

        post['language']='de'
        data.append(post)

    data=np.asarray(data)

    train_data, val_data=train_test_split(data, test_size=0.2, 
                                                  random_state=42)
    csvfile.close()

    csvfile=open('../data/germeval/germeval2018.test.txt','r')
    reader=csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    test_data=[]
    for line in reader:
        post={}
        post['text']=line[0]
        if(categorical):
            post['abuse']=int(line[2]=='ABUSE')
            post['insult']=int(line[2]=='INSULT')
            post['profanity']=int(line[2]=='PROFANITY')
            post['other']=int(line[2]=='OTHER')
        else:
            post['offensive']=int(line[1]=='OFFENSE')

        post['language']='de'
        test_data.append(post)

    test_data=np.asarray(test_data)

    return train_data, val_data, test_data

def get_data_germeval2019_task3():
    csvfile=open('../data/germeval/germeval2019_training_subtask3.txt','r')
    reader=csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    data=[]
    for line in reader:
        post={}
        post['text']=line[0]
        post['implicit']=int(line[3]=='IMPLICIT')

        post['language']='de'
        data.append(post)

    data=np.asarray(data)

    train_data, test_data=train_test_split(data, test_size=0.4, 
                                                  random_state=42)
    csvfile.close()
    return train_data, test_data

def get_data_germeval2018(categorical):
    csvfile=open('../data/germeval/germeval2018.training.txt','r')
    reader=csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    data=[]
    for line in reader:
        post={}
        post['text']=line[0]
        if(categorical):
            post['abuse']=int(line[2]=='ABUSE')
            post['insult']=int(line[2]=='INSULT')
            post['profanity']=int(line[2]=='PROFANITY')
            post['other']=int(line[2]=='OTHER')
        else:
            post['offensive']=int(line[1]=='OFFENSE')

        post['language']='de'
        data.append(post)

    data=np.asarray(data)

    train_data, val_data=train_test_split(data, test_size=0.2, 
                                                  random_state=42)
    csvfile.close()

    csvfile=open('../data/germeval/germeval2018.test.txt','r')
    reader=csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    test_data=[]
    for line in reader:
        post={}
        post['text']=line[0]
        if(categorical):
            post['abuse']=int(line[2]=='ABUSE')
            post['insult']=int(line[2]=='INSULT')
            post['profanity']=int(line[2]=='PROFANITY')
            post['other']=int(line[2]=='OTHER')
        else:
            post['offensive']=int(line[1]=='OFFENSE')

        post['language']='de'
        test_data.append(post)

    test_data=np.asarray(test_data)

    return train_data, val_data, test_data

def get_data_germeval2018_reduced():
    import pickle
    reduced_train_file = open('../data/germeval/resampled.pickle','rb')
    reduced_train_data=pickle.load(reduced_train_file)
    _,val_data,test_data=get_data_germeval(False)
    return reduced_train_data, val_data, test_data

def get_data_evalitamixed(twtofb, fbtotw):
    twitter_train_data, twitter_validation_data, twitter_test_data=get_data_evalita_twitter()
    fb_train_data, fb_validation_data, fb_test_data=get_data_evalita_facebook()

    if(twtofb):
        return twitter_train_data, fb_validation_data, fb_test_data
    elif(fbtotw):
        return fb_train_data, twitter_validation_data, twitter_test_data
    else:
        all_train=np.concatenate((twitter_train_data, fb_train_data), axis=0)
        all_validate=np.concatenate((twitter_validation_data,
                                     fb_validation_data), axis=0)
        all_test=np.concatenate((twitter_test_data, fb_test_data),axis=0)

        return all_train, all_validate, all_test

def get_data_evalita_facebook():
    """Read facebook hatespeech italian dataset from evalita task
        
        Returns:
            a tuple containing training,test data
    """
    import csv
    
    csvfile=open('../data/evalita18/hate-speech/haspeede_FB-train.tsv')
    reader=csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    data=[]
    for line in reader:
        post={}
        post['text']=line[1]
        post['hate-speech']=int(line[2])
        post['language']='it'
        data.append(post)

    data=np.asarray(data)

    train_data, validation_data=train_test_split(data, test_size=0.2,
                                                  random_state=42)

    csvfile=open('../data/evalita18/hate-speech/haspeede_FB-reference.tsv')
    reader=csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    test_data=[]
    for line in reader:
        post={}
        post['text']=line[1]
        post['hate-speech']=int(line[2])
        post['language']='it'
        test_data.append(post)

    test_data=np.asarray(test_data)

    return train_data, validation_data, test_data


def get_data_evalita_twitter():
    """Read twitter hatespeech italian dataset from evalita task
        
        Returns:
            a tuple containing training,test data
    """
    import csv
    
    csvfile=open('../data/evalita18/hate-speech/haspeede_TW-train.tsv')
    reader=csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    data=[]
    for line in reader:
        tweet={}
        tweet['text']=line[1]
        tweet['hate-speech']=int(line[2])
        tweet['language']='it'
        data.append(tweet)
    
    data=np.asarray(data)

    train_data, validation_data=train_test_split(data, test_size=0.2, 
                                                  random_state=42)

    csvfile=open('../data/evalita18/hate-speech/haspeede_TW-reference.tsv')
    reader=csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
    test_data=[]
    for line in reader:
        tweet={}
        tweet['text']=line[1]
        tweet['hate-speech']=int(line[2])
        tweet['language']='it'
        test_data.append(tweet)
    
    test_data=np.asarray(test_data)

    return train_data, validation_data, test_data

def get_data_twitter_ita():
    """Read twitter hatespeech italian dataset
        
        Returns:
            a tuple containing training,test data
    """
    import json
    from nltk.sentiment.vader import SentimentIntensityAnalyzer as VS
    json_file = open('../data/hate-speech-italian/tweets.json', 'r')
    json_data=json.load(json_file)
    sentiment_analyzer=VS()
    data=[]
    for t in json_data:
        tweet={}
        text=t['text']
        sentiment=sentiment_analyzer.polarity_scores(text)
        tweet['text']=text
        tweet['sentiment']=float(sentiment['compound'])
        tweet['hate-speech']=int(t['hatespeech']=='yes')
        tweet['language']='it'
        data.append(tweet)

    data=np.asarray(data)
    
    train_data, test_data=train_test_split(data, test_size=0.4, 
                                                  random_state=42)
    return train_data, test_data

def get_data_twitter_itaeng():
    """Import waseem and italian mixed dataset
        
        Returns:
            a tuple containing training,test data
    """
    train_data_ita, test_data_ita=get_data_twitter_ita()
    train_data_eng, test_data_eng=get_data_evalita_twitter()

    validation_input_no=round(len(test_data_ita)*0.5)
    validation_data_ita= test_data_ita[:validation_input_no]
    test_data_ita=test_data_ita[validation_input_no:]

    validation_input_no=round(len(test_data_eng)*0.5)
    validation_data_eng=test_data_eng[validation_input_no:]

    train_data_merged=np.concatenate((train_data_eng,train_data_ita))
    validation_data_merged=np.concatenate((validation_data_eng,validation_data_ita))

    return train_data_merged, validation_data_ita, test_data_ita

def clean_instagram_text(text):
    """Remove tags from instagram tex

        Args:
            instagram text

        Returns:
            text without tags
    """
    import re
    import string
    #remove the tags and the (created at...) expression
    rexp_string=r'<[^>]+>|\(created[^)]+\)'
    tags_created_rexp = re.compile(rexp_string)
    text_cleaned = tags_created_rexp.sub(" ", text)
    #remove words made only by symbols (or underscores)
    text_no_symbol_words=re.sub(r'(^| )([^\w\s]|[\_])+(?=$| )',"",text_cleaned)
    return text_no_symbol_words

def clean_instagram_user(user):
    """
        Remove tags from instagram user string
        
        Args:
            user: user string from instagram dataset

        Returns:
            user string without tags

    """
    import re
    tags_spaces_rexp=re.compile(r'<[^>]+>|[ ]+')
    user=tags_spaces_rexp.sub("", user)
    return user

def count_mentions(text):
    """Count user mentions (@user) in a string

        Args:
            text: some text from a social network
        
        Returns:
            the number of mentions in the text
    """
    import re
    mentions_re = re.compile(r'(?:^|\s)[@](\w+)')
    mentions = mentions_re.findall(text)
    return len(mentions)

def instagram_get_user_text(string):
    """Get user and text without tags

    Args:
        The post string

    Returns:
        A tuple containing (user, text)
    """
    # text and user are separated by a triple space
    nameandtext = string.split("  ")
    user=clean_instagram_user(nameandtext[0])
    
    text=clean_instagram_text(" ".join(nameandtext[1:]))
    return user, text

def get_data_instagram():
    """ Get instagram dataset training/test data
        
        Returns:
            A tuple containing training/test set
    """
    from nltk.sentiment.vader import SentimentIntensityAnalyzer as VS

    from io import StringIO
    import re
    import json
    from multiprocessing import Pool
    import spacy

    json_filename="../data/instagram-cyberbullying/all_instagram_sessions_with_images.json"
    data=[]
    positive_instances=0
    sentiment_analyzer=VS()

    p=Pool()

    jsonfile=open(json_filename, "r")

    json_data=json.load(jsonfile)

    nlp=spacy.load('en',disable=['tagger','parser','ner','textcat'])

    for row in json_data:
        session={}
        users=[]
        # get text both with and without symbols for the owner post
        session_text=clean_instagram_text(row['owner_comment'][0])
        session_text_symbol=row['owner_comment'][0]

        # get the owner id (without html tags)
        owner=clean_instagram_user(row['owner_id'])
        # append the owner id for each word that is part of this comment
        users.append(owner)

        #cycle through each column (comment)
        session['comments'] = [session_text]
        # sentiment_comment = sentiment_analyzer.polarity_scores(session_text)
        #session['sentiment_comments']=[float(sentiment_comment['compound'])]
        session['emotion_comments']=[row['owner_comment'][1]['emotion:confidence']]
        for comment in row['columns']:
            # populate text, text with symbols and users 
            user, text=instagram_get_user_text(comment[0])
            text_symbol_words = comment[0]

            #sentiment_comment = sentiment_analyzer.polarity_scores(session_text)
            #session['sentiment_comments'].append(float(sentiment_comment['compound']))
            session['emotion_comments'].append(comment[1]['emotion:confidence'])

            users.append(user)
            session_text+=" "+text
            session_text_symbol+=" "+text_symbol_words
            session['comments'].append(text)
        
        #populate the session dictionary
        session['text']=session_text
        session['text_symbols']=session_text_symbol
        session['users']=users
        sentiment=sentiment_analyzer.polarity_scores(session_text)
        session['sentiment']=float(sentiment['compound'])
        session['language']='en'
        
        sentiments=p.map(sentiment_analyzer.polarity_scores, session['comments'])
        
        session['sentiment_comments']=[float(s['compound']) for s in sentiments]

        session['img']={}
        session['img']['creender']={}
        session['img']['sentiment']={}

        session['img']['creender']['prediction']=row['creender']['prediction']
        session['img']['creender']['descriptor']=row['creender']['descriptor']

        session['img']['sentiment']['prediction']=row['sentiment']['prediction']
        session['img']['sentiment']['descriptor']=row['sentiment']['descriptor']

        session['cyberbullying']=int(row['bullying_label']=='bullying' and 
                                     float(row['bullying:confidence'])>=0.6)

        session['num-posts']=len(session['comments'])

        #DEBUG
        positive_instances+=session['cyberbullying']
        data.append(session)
    #DEBUG
    print('{} positive instances out of {}'.format(positive_instances, len(data)))
    data=np.array(data)
    p.close()
    p.join()
    train_data,test_data=train_test_split(data, test_size=0.4, random_state=42)
    return train_data, test_data

def get_data_bullying():
    """ Get twitter bullying traces dataset training/test data
        
        Returns:
            A tuple containing training/test set
    """
    from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer as VS

    with open('../data/bullying/bullyinv3_englishonly.csv') as csvfile:
        data=[]
        sentiment_analyzer=VS()
        reader=csv.DictReader(csvfile)
        for row in reader:
            tweet={'text':row['tweets'], 
                   'bullying trace': int(row['bullying_traces']=='y')}
            tweet['language']='en'
            sentiment=sentiment_analyzer.polarity_scores(tweet['text'])
            tweet['sentiment']=float(sentiment['compound'])
            data.append(tweet)
    data=np.array(data)
    train_data,test_data=train_test_split(data,test_size=0.4,random_state=42)
    return train_data, test_data

def get_data_hatespeech():
    """ Get twitter hate speech dataset training/test data
        
        Returns:
            A tuple containing training/test set
    """
    from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer as VS
    with open('../data/hate-speech/labeled_data.csv') as csvfile:
        data=[]
        sentiment_analyzer=VS()
        reader=csv.DictReader(csvfile)
        for row in reader:
            tweet={'text':row['tweet'],'hate-speech':int(int(row['class'])<2)}
            tweet['language']='en'
            sentiment = sentiment_analyzer.polarity_scores(row['tweet'])
            tweet['sentiment']=float(sentiment['compound'])
            data.append(tweet)
    train_data,test_data=train_test_split(data,test_size=0.1,random_state=42)
    return train_data,test_data

def get_data_waseem(categorical=True):
    """ Get twitter waseem hate speech dataset training/test data
        
        Args:
            categorical: a boolean to determine whether to return binary 
            or categorical fields

        Returns:
            A tuple containing training/test set
    """
    from random import shuffle
    from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer as VS
    with open('../data/hatespeech-WaseemHovy/NAACL_SRW_2016_tweets.txt') as csvfile:
        data=[]
        sentiment_analyzer=VS()
        reader=csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
        for row in reader:
            tweet={'text':row[2]}
            tweet['language']='en'
            if(categorical):
                tweet['racism']=int(row[1]=='racism')
                tweet['sexism']=int(row[1]=='sexism')
                tweet['none']=int(row[1]=='none')
            else:
                tweet['hate-speech']=int(row[1]=='racism' or row[1]=='sexism')
            sentiment = sentiment_analyzer.polarity_scores(tweet['text'])
            tweet['sentiment']=float(sentiment['compound'])
            data.append(tweet)
    data=np.array(data)
    train_data,test_data=train_test_split(data, test_size=0.4, random_state=42)

    # validation data is half of the testing data (20% of complete dataset)
    validation_input_no = int(round(len(test_data)*0.5))
    
    validation_data = test_data[:validation_input_no]

    test_data=test_data[validation_input_no:]
    return train_data, validation_data, test_data

def get_data_waseem_reduced():
    import pickle
    reduced_train_file=open('../data/hatespeech-WaseemHovy/resampled.pickle','rb')
    reduced_train_data=pickle.load(reduced_train_file)
    reduced_train_file.close()
    _, test_data=get_data_waseem(False)
    return reduced_train_data, test_data

def get_data_argumentative(folder_name,dataset_names,
                           sentiment_data_source='alchemy'):
    """ Get twitter argumentative dataset training/test data
        
        Args:
            folder_name: folder where the dataset is located;
            dataset_names: names for the datasets
            sentiment_data_source: either alchemy or vader
            
        Returns:
            A tuple containing training/test set
    """
    from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer as VS
    data=[]
    for name in dataset_names:
        json_data = open(folder_name + name+'_ann' + '.json')
        data_curr=(json.load(json_data))
        if(sentiment_data_source=='alchemy'):
            alchemy_data = open(folder_name + name + '.alchemy')
        for i in range(len(data_curr)):
            if(sentiment_data_source=='alchemy'):
                line = alchemy_data.readline()
                elems = line.split()
                data_curr[i]["sentiment"] = float(elems[1])
            elif(sentiment_data_source=='vader'):
                sentiment_analyzer=VS()
                sentiment=sentiment_analyzer.polarity_scores(data_curr[i]['text'])
                data_curr[i]["sentiment"]=float(sentiment['compound'])
            data_curr[i]["argument"]=int(data_curr[i]['argument']=='YES')
        data.extend(data_curr)
        json_data.close()
        if(sentiment_data_source=='alchemy'):
            alchemy_data.close()
    return(data)

def get_labels(data, fields):
    """ Get the labels from a given dataset
        
        Args:
            data: data to extract labels from
            fields: the fields for the specific dataset
            
        Returns:
            A numpy array containing the labels
    """

    l = []
    n=len(data)
    positive=np.zeros(len(fields),dtype=np.int32)
    for t in data:
        if(len(fields)>2):
            result=np.array([t[field] for field in fields])
        else:
            result=np.array([t[fields[-1]]])
        positive+=result
        l.append(result) 
    return(np.asarray(l))


def get_unlabeled_data(filename, text_field_no, language):
    """ Read data from unlabeled file

        Args:
            filename: csv filename

        Returns:
            a numpy array containg the data in the classifier format
    """
    datafile = open(filename, 'r')
    csvfile=csv.reader(datafile, delimiter='\t',quoting=csv.QUOTE_NONE)
    data=[]
    for line in csvfile:
        curr_dict={}
        curr_dict['text']=line[text_field_no]
        curr_dict['language']='en'
        curr_dict['other_fields']=line[:text_field_no]+line[text_field_no+1:]
        data.append(curr_dict)

    data = np.asarray(data)
    return data
