import re
from preprocess.urlregex import WEB_URL_REGEX
import emoji
import functools
from functools import lru_cache
import operator

italian_accents_fix=[(r"a['|`]",'à'),(r"e['|`]",'è'),(r"i['|`]",'ì'),
                     (r"o['|`]",'ò'),(r"u['|`]",'ù')]

punctuation_list=["!","?"]

emoji_translations={}

all_emojis_re=None

def escape_word_h5py(word):
    import re
    """Escape a word to fit in the h5py format

    Args:
        word: the word to escape
    """
    if(len(word)==0):
        word='__emptystr__'
    elif(word=='.'):
        word='__dot_symbol__'
    elif('/' in word):
        word=re.sub('\/', '__slash_symbol__', word)
    return word

def decode_emoji(emoji, emoji_dict):
    emoji_hex=emoji[4:]
    if(emoji_hex in emoji_dict):
        decoded_str=emoji_dict[emoji_hex]
    else:
        decoded_str=chr(int(emoji_hex,16))

    return decoded_str

def populate_emoji_translations(lang):
    import pickle
    emoji_dict_file=open('../data/emoji-translations/en2{}'.format(lang),'rb')
    emoji_translations[lang]=pickle.load(emoji_dict_file)
    emoji_dict_file.close()

def get_emoji_translation(doc, lang):
    global all_emojis_re

    if(lang not in emoji_translations):
        populate_emoji_translations(lang)

    if(all_emojis_re is None):
        all_emojis_re=re.compile('('+'|'.join(emoji_translations[lang].keys())+')')

    doc=all_emojis_re.sub(lambda x: emoji_translations[lang][x.group(0)], doc)
    return doc

@lru_cache(maxsize=10000)
def normalize_string(doc, language, emoji2text=False, for_embeddings=False):
    
    # every url becomes the string URL
    doc=re.sub(WEB_URL_REGEX, "URL", doc)

    # for italian, separate apostrophed words
    if(language=='it'):
        doc=re.sub(r"(\w)['|’](\w|#)", r"\1' \2", doc)
        for pattern, substitution in italian_accents_fix:
            doc=re.sub(pattern,substitution,doc)

    #translate emojis to words (do we want to keep the :?)
    if(emoji2text):
        doc=emoji.demojize(doc)
        if(language=='en'):
            doc=re.sub(r'\:([^:]+)\:', r" \1 ",doc)
            doc=re.sub(r'_', r" ", doc)
        else:
            doc=get_emoji_translation(doc,language)
   
    #separate words divided by / and - (FIXME: what about \)
    if(for_embeddings):
        doc=re.sub(r"(\w)([/|\-]+)(\w)", r'\1 \2 \3',doc)

    # keep only one punctuation (!!! -> !)
    for pattern in punctuation_list:
        doc=re.sub(r"[{}]+".format(pattern),r"{}".format(pattern),doc)
    
    # space between emojis
    emoji_re=emoji.get_emoji_regexp()
    split_emoji=emoji_re.split(doc)
    split_whitespace = [substr.split() for substr in split_emoji]
    split_emoji = functools.reduce(operator.concat, split_whitespace)
    doc=" ".join(split_emoji)
    
    
    # substitute usernames
    doc=re.sub(r'(^|\s)@(\w+)', r"\1username",doc)
    
    if(not for_embeddings):
        doc=re.sub(r'(^|\s)[＃#](\w+)',r"\1 \2", doc)
        doc=doc.lower()

    # remove multiple whitespaces
    doc=re.sub(r'  +', " ", doc)
    
    return doc
