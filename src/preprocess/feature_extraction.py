#!/usr/bin/env python3

import numpy as np
import h5py
import spacy
from functools import lru_cache

from resources.Embeddings import Embeddings, FastTextEmbeddings
from preprocess.text_normalization import normalize_string

nlp_multilingual={}

def get_all_features(data, model_conf):
    """Get all the specified features for a given ModelConfiguration

    Args:
        data: the data we want to extract the features from
        model_conf: the current ModelConfiguration

    Returns:
        a list of tensor representing the requested features
    """

    features=[]

    if("word2vec" in model_conf.text_features):
        curr_embeddings = get_word2vec_embeddings(data, model_conf)
        features.append(curr_embeddings)
        if(model_conf.train_dataset_name == 'instagram-cyberbullying'):
            curr_speakers = get_users(model_conf, data, curr_embeddings.shape[1])
            features.append(curr_speakers)


    if('char-rnn' in model_conf.text_features):
        curr_char_tensor=get_char_tensor(data, model_conf)
        features.append(curr_char_tensor)

    if("cbows" in model_conf.text_features):
        cbow_sums, cbow_avgs = get_cbows_sum_avg(data,model_conf)
        features.append(cbow_sums)
        features.append(cbow_avgs)

    if(model_conf.use_ngrams()):
        ngrams=get_ngrams(data, model_conf)
        features.append(ngrams)

    if(model_conf.use_pos()):
        pos_ngrams=get_pos(data, model_conf)
        features.append(pos_ngrams)

    if('emolex' in model_conf.other_features):
        curr_emolex=get_emolex(data, model_conf)
        features.append(curr_emolex)

    if('hurtlex' in model_conf.other_features):
        curr_hurtlex=get_hurtlex(data, model_conf)
        features.append(curr_hurtlex)

    if('emotion' in model_conf.other_features):
        curr_emotion=get_emotions(data)
        features.append(curr_emotion)

    if("sentiment" in model_conf.other_features):
        if(model_conf.dataset_name=='instagram-cyberbullying'):
            curr_sentiment=get_sentiment_posts(data)
        else:
            curr_sentiment = get_sentiment_data(data)
        features.append(curr_sentiment)

    if("social" in model_conf.other_features):
        curr_social = get_social_features(data)
        normalize_features(curr_social, model_conf.means, model_conf.stds)
        features.append(curr_social)

    if("img_creender_predictions" in model_conf.other_features):
        img_feature = get_instagram_image_info(data, 'creender','prediction')
        features.append(img_feature)

    if("img_creender_descriptors" in model_conf.other_features):
        img_feature = get_instagram_image_info(data, 'creender','descriptor')
        features.append(img_feature)

    if("img_sentiment_predictions" in model_conf.other_features):
        img_feature = get_instagram_image_info(data, 'sentiment', 'prediction')
        features.append(img_feature)

    if("img_sentiment_descriptors" in model_conf.other_features):
        img_feature = get_instagram_image_info(data, 'sentiment','descriptor')
        features.append(img_feature)

    return features


def get_char_tensor(data, model_conf):
    """Get a character tensor for the char-level RNN based models

    Args:
        data: data to extract features
        mdoel_conf: ModelConfiguration object

    Returns:
        a char tensor where every char is represented by an integer
    """

    # the insta2vec feature uses tensor of the form (num_media_sessions,
    # num_comments, num_chars) 
    insta2vec='insta2vec' in model_conf.text_features
    if(insta2vec):
        max_num_posts=0
        for post in data:
            max_num_posts=max(max_num_posts, len(post['comments']))
        char_tensor=np.full((len(data), max_num_posts ,model_conf.max_num_chars),-1)
    # all other char-based features use the form (num_posts, num_chars)
    else:
        char_tensor=np.full((len(data),model_conf.max_num_chars),-1)

    for p_idx,post in enumerate(data):
        # get insta2vec tensors
        if(insta2vec):
            for comment_idx,comment in enumerate(post['comments']):
                for char_idx,char in enumerate(comment.lower()):
                    if(char_idx>=model_conf.max_num_chars):
                        break
                    if(char in model_conf.char2int):
                        char_tensor[p_idx,comment_idx,char_idx]=model_conf.char2int[char]
        # get other kinds of char-rnn tensor
        else:
            for char_idx,char in enumerate(post['text'].lower()):
                if(char in model_conf.char2int):
                    char_tensor[p_idx,char_idx]=model_conf.char2int[char]
    return char_tensor

def get_users(model_conf, data, max_num_words):
    """Get user tensors for instagram. Each user is represented by a one-hot
    encoding. The index for a given user is computed on a single media-session,
    so that indices are local to that media session.
    (eg the same user in different media sessions can have different indices)

    Args:
        data: data to extract feature
        max_num_words: maximum words in a post of the dataset

    Returns:
        a one-hot encoding for users
    """
    transcribe_emojis=(model_conf.emojis=='emoji-transcription')

    users=np.full((len(data),max_num_words,197),-1)

    for post_idx in range(len(data)):
        post=data[post_idx]
        lang=post['language']

        count_users=0
        j=0
        user_set={}
        for i, comment in enumerate(post['comments']):

            doc=normalize_string(comment,lang, transcribe_emojis, True)

            tokenizedtext=tokenize_string(doc, lang)
            curr_user=post['users'][i]

            for token in range(len(tokenizedtext)):

                if(curr_user in user_set):
                    user_idx=user_set[curr_user]
                    users[post_idx,j,user_idx]=1
                else:
                    user_set[curr_user]=count_users
                    users[post_idx,j,count_users]=1
                    count_users+=1
                j+=1
    return users

def get_feature_means_stds(features):
    """Get means and stds for a feature array

    Args:
        features: a feature matrix with size (num-samples, num-features)

    Returns:
        means,stds for the feature array (by column)
    """
    features_range=range(features.shape[1])
    means=[np.mean(features[:,i]) for i in features_range]
    stds=[np.std(features[:,i]) for i in features_range]
    return means,stds

def get_social_features(data):
    """Get social features (number of hashtags, number of mentions,
    number of uppercase words, exlamation marks, question marks, emojis

    Args:
        data: data to get the social features

    Return:
        social features array
    """
    # get the social feature matrix from the tweet
    import re
    import emoji
    hashtags_re = re.compile(r'(?:^|\s)[＃#](\w+)')
    mentions_re = re.compile(r'(?:^|\s)[@](\w+)')
    uppercase_re = re.compile(r'(?:\b)[A-Z]+(?:\b)')
    exclamation_re = re.compile(r'!')
    question_re = re.compile(r"\?")
    emojis_list = [e for x in emoji.UNICODE_EMOJI.keys() for e in x.split()]
    emoji_re = emoji.get_emoji_regexp()
    rexp_list = [hashtags_re, mentions_re,
                 uppercase_re, exclamation_re,
                 question_re, emoji_re]

    #for instagram, we include the number of posts in each media session
    use_num_posts=int("num-posts" in data[0])

    features=np.zeros((len(data), len(rexp_list)+use_num_posts))

    # for the instagram dataset we need to use the text_symbols field from 
    # data
    if 'text_symbols' in data[0]:
        data_field='text_symbols'
    else:
        data_field='text'

    for t_idx in range(len(data)):
        for r_idx in range(len(rexp_list)):
            features[t_idx, r_idx] = (len(rexp_list[r_idx].findall(
                                     data[t_idx][data_field])))
        if(use_num_posts==1):
            features[t_idx,-1]=data[t_idx]["num-posts"]

    return features

def tokenize_normalize_only(data, model_conf):
    transcribe_emojis=(model_conf.emojis=='emoji-transcription')
    tokenized=[]
    for tweet in data:
        doc=tweet['text']
        lang=tweet['language']
        doc=normalize_string(doc,lang, transcribe_emojis, True)

        tokenized.append(tokenize_string(doc, lang))

    return tokenized

def get_word2vec_embeddings(data, model_conf):
    """Get word embeddings for the given dataset

    Args:
        data: dataset
        model_conf: ModelConfiguration object

    Returns:
        a tensor of word embeddings with size (num_posts,num_words,embedding_size)
    """
    from multiprocessing import Pool
    import re
    import emoji
    from preprocess import urlregex

    transcribe_emojis=(model_conf.emojis=='emoji-transcription')

    if('fasttext' in model_conf.word2vec_source):
        embeddings=FastTextEmbeddings('../data/embeddings/fasttext/{}.bin'.format(model_conf.word2vec_source))
    else:
        # Open the word embedding file for the specified source and create a 
        # Embeddings instance
        h5py_file = h5py.File('../data/embeddings/{}/embeddings.hdf5'\
                              .format(model_conf.word2vec_source),'r')

        # create a Embeddings object, specify the language if we're using multilingual
        # embeddings
        if(model_conf.word2vec_source=='multilingual'):
            embeddings_multilang={}
            embeddings_multilang['en'] = Embeddings.from_h5py_file(h5py_file,'en')
            embeddings_multilang['it'] = Embeddings.from_h5py_file(h5py_file,'it')
        elif(model_conf.word2vec_source=='multilingual-ger'):
            embeddings_multilang={}
            embeddings_multilang['de'] = Embeddings.from_h5py_file(h5py_file,'de')
            embeddings_multilang['en'] = Embeddings.from_h5py_file(h5py_file,'en')
            embeddings_multilang['emoji']=Embeddings.from_h5py_file(h5py_file,'emoji')
        elif(model_conf.word2vec_source=='multilingual-pivot'):
            embeddings_multilang={}
            embeddings_multilang['de'] = Embeddings.from_h5py_file(h5py_file,'de')
            embeddings_multilang['en'] = Embeddings.from_h5py_file(h5py_file,'en')
            embeddings_multilang['it'] = Embeddings.from_h5py_file(h5py_file,'it')
            embeddings_multilang['emoji']=Embeddings.from_h5py_file(h5py_file,'emoji')
        else:
            embeddings = Embeddings.from_h5py_file(h5py_file)

    num_tweets = len(data)
    max_num_words = 0
    emoji_re=emoji.get_emoji_regexp()

    for tweet in data:
        doc=tweet['text']
        lang=tweet['language']
        doc=normalize_string(doc,lang, transcribe_emojis, True)

        tokenizedtext=tokenize_string(doc, lang)
        max_num_words = max(max_num_words, len(tokenizedtext))

    # Create an embedding matrix in order to accomodate every vector
    curr_embeddings = np.full((num_tweets, max_num_words, 
                               model_conf.embedding_size),-1.)

    for tweet_idx in range(num_tweets):
        #normalize text
        lang=data[tweet_idx]['language']

        if('multilingual' in model_conf.word2vec_source):
            embeddings=embeddings_multilang[lang]

        doc=data[tweet_idx]['text']
        doc=normalize_string(doc, lang, transcribe_emojis, True)

        # Tokenize the tweet and get the word embeddings
        words=tokenize_string(doc,lang)
        for word_idx in range(len(words)):
            word = words[word_idx]
            if(model_conf.emojis=='vec_emoji' and 'emoji' in embeddings_multilang and emoji_re.match(word) is not None):
                embedding=embeddings_multilang['emoji'].get_word_vector(word)
                continue
            else:
                embedding=embeddings.get_word_vector(word, True)
            curr_embeddings[tweet_idx, word_idx, :] = embedding
    if('multilingual' in model_conf.word2vec_source):
        del(embeddings_multilang)
    else:
        del(embeddings)

    if('fasttext' not in model_conf.word2vec_source):
        h5py_file.close()

    return curr_embeddings

def get_cbows_sum_avg(data, model_conf):
    """ Get sums and averages of the word embeddings for each post in the
    dataset

    Args:
        data: the provided dataset
        model_conf: a ModelConfiguration object

    Returns:
        an array representing the sums and averages of the word embeddings
        with size (num_posts, 2*embedding_size)
    """
    transcribe_emojis=(model_conf.emojis=='emoji-transcription')

    if('fasttext' in model_conf.word2vec_source):
        embeddings=FastTextEmbeddings('../data/embeddings/fasttext/{}.bin'.format(model_conf.word2vec_source))
    else:
        # Open the word embedding file for the specified source and create a 
        # Embeddings instance
        h5py_file = h5py.File('../data/embeddings/{}/embeddings.hdf5'\
                              .format(model_conf.word2vec_source),'r')

        # create a Embeddings object, specify the language if we're using multilingual
        # embeddings
        if(model_conf.word2vec_source=='multilingual'):
            embeddings_multilang={}
            embeddings_multilang['en'] = Embeddings.from_h5py_file(h5py_file,'en')
            embeddings_multilang['it'] = Embeddings.from_h5py_file(h5py_file,'it')
        elif(model_conf.word2vec_source=='multilingual-ger'):
            embeddings_multilang={}
            embeddings_multilang['de'] = Embeddings.from_h5py_file(h5py_file,'de')
            embeddings_multilang['en'] = Embeddings.from_h5py_file(h5py_file,'en')
            embeddings_multilang['emoji']=Embeddings.from_h5py_file(h5py_file,'emoji')
        elif(model_conf.word2vec_source=='multilingual-pivot'):
            embeddings_multilang={}
            embeddings_multilang['de'] = Embeddings.from_h5py_file(h5py_file,'de')
            embeddings_multilang['en'] = Embeddings.from_h5py_file(h5py_file,'en')
            embeddings_multilang['it'] = Embeddings.from_h5py_file(h5py_file,'it')
            embeddings_multilang['emoji']=Embeddings.from_h5py_file(h5py_file,'emoji')
        elif('fasttext' in model_conf.word2vec_source):
            embeddings = Embeddings.from_h5py_file(h5py_file)

    num_tweets = len(data)

    cbow_sums=np.zeros((num_tweets,model_conf.embedding_size))
    cbow_avgs=np.zeros((num_tweets,model_conf.embedding_size))

    for tweet_idx in range(num_tweets):
        post=data[tweet_idx]
        lang=post['language']
        text=post['text']

        # Tokenize the tweet and get the word embeddings sum and avg
        if('multilingual' in model_conf.word2vec_source):
            embeddings=embeddings_multilang[lang]

        doc=normalize_string(text,lang, transcribe_emojis, True)

        words = tokenize_string(doc, lang)
        curr_sum=np.zeros((model_conf.embedding_size,))
        curr_avg=np.zeros((model_conf.embedding_size,))
        num_words=len(words)
        for word_idx in range(len(words)):
            word = words[word_idx]
            embedding=embeddings.get_word_vector(word, True)
            curr_sum += embedding
            curr_avg += embedding
        curr_sum=embeddings.normalize_vector(curr_sum)
        if(num_words==0):
            num_words=1
        curr_avg=embeddings.normalize_vector(curr_avg/num_words)

        cbow_sums[tweet_idx]=curr_sum
        cbow_avgs[tweet_idx]=curr_avg

    return cbow_sums, cbow_avgs

def normalize_features(features, means, stds):
    """Normalize each feature (by column in the matrix) by subtracting the mean
    and dividing by the std

    Args:
        features: feature matrix (each column is a feature)
        means: features means
        stds: features stds
    """

    import numpy as np

    for i in range(features.shape[1]):
        features[:, i] = (features[:, i]-means[i])/max(stds[i],[1])

    return

def get_pos(data, model_conf):
    """Get POS feature matrix for the given data and vectorizer

    Args:
        data: data to extract POS ngrams from
        model_conf: module configuration

    Returns:
        vectorized POS ngrams
    """
    if(model_conf.dataset_name == 'instagram-cyberbullying'):
        counted=[]
        for media_session in data:
            curr_lang=media_session['language']
            pos_tags=[]
            for comment in tagged_comments:
                curr_pos_list=pos_string(comment, lang)
                pos_tags.append(" ".join(curr_pos_list))
            curr_msess = model_conf.pos_vectorizer.transform(pos_tags).toarray()
            counted.append(curr_msess.sum(axis=0))
    else:
        pos_tags=[]
        for tweet in data:
            curr_lang=tweet['language']
            tweet_tags = pos_string(tweet['text'], curr_lang)
            tweet_tags = " ".join(tweet_tags)
            pos_tags.append(tweet_tags) 
        counted=model_conf.pos_vectorizer.transform(pos_tags)

    vectorized_tags=model_conf.pos_transformer.transform(counted).toarray()
    return vectorized_tags


def get_ngrams(data, model_conf):
    """Get ngram feature matrix for the given data and vectorizer

    Args:
        data: data to extract ngrams from
        model_conf: module configuration

    Returns:
        vectorized ngrams
    """
    vectorizer=model_conf.ngram_vectorizer
    transformer=model_conf.ngram_transformer
    transcribe_emojis=(model_conf.emojis=='emoji-transcription')

    if(model_conf.dataset_name == 'instagram-cyberbullying'):
        ngrams=[]
        for media_session in data:
            curr_lang=media_session['language']
            for comment in media_session['comments']:
                curr_comment=normalize_string(tweet['text'], curr_lang, 
                                              transcribe_emojis)
                curr_lemmas=lemmatize_string(curr_comment, curr_lang)
                lemmas_posts.append(" ".join(curr_lemmas))
            per_post=vectorizer.transform(lemmas_posts).toarray()
            msess_sum=per_post.sum(axis=0)
            ngrams.append(msess_sum)
    else:
        lemma_tweets=[]
        for tweet in data:
            curr_lang=tweet['language']
            curr_text=normalize_string(tweet['text'], curr_lang, 
                                       transcribe_emojis) 
            curr_lemmas=lemmatize_string(curr_text, curr_lang)
            lemma_tweets.append(" ".join(curr_lemmas))

        ngrams=vectorizer.transform(lemma_tweets)

    ngrams=transformer.transform(ngrams).toarray()
    return ngrams

def get_emolex(data,model_conf, tfidf=True):
    from preprocess.text_normalization import escape_word_h5py

    transcribe_emojis=(model_conf.emojis=='emoji-transcription')

    emolex_file=h5py.File('../data/emotion-lexicon/lexicon.hdf5','r')
    transformer=model_conf.emolex_transformer

    emolex_matrix=np.zeros((len(data),10))
    num_tokens=[]
    for idx,tweet in enumerate(data):
        curr_lang=tweet['language']
        curr_text=normalize_string(tweet['text'], curr_lang, 
                                   transcribe_emojis)
        curr_tokens=tokenize_string(curr_text, curr_lang)
        curr_emolex_lang=emolex_file[curr_lang]
        num_tokens.append(len(curr_tokens))
        for token in curr_tokens:
            token_escaped=escape_word_h5py(token)
            if(token_escaped in curr_emolex_lang):
                emolex_matrix[idx]+=curr_emolex_lang[token_escaped.lower()]
    if(tfidf):
        emolex_matrix=transformer.transform(emolex_matrix).toarray()
    else:
        for i in range(len(emolex_matrix)):
            emolex_matrix[i,:]=emolex_matrix[i,:]/num_tokens[i]

    return emolex_matrix


def get_hurtlex(data,model_conf, tfidf=True):
    from preprocess.text_normalization import escape_word_h5py

    transcribe_emojis=(model_conf.emojis=='emoji-transcription')

    hurtlex_file=h5py.File('../data/hurtlex/lexicon.hdf5','r')
    transformer=model_conf.hurtlex_transformer

    hurtlex_matrix=np.zeros((len(data),19))
    num_tokens=[]
    for idx,tweet in enumerate(data):
        curr_lang=tweet['language']
        curr_text=normalize_string(tweet['text'], curr_lang, 
                                   transcribe_emojis)
        curr_tokens=tokenize_string(curr_text, curr_lang)
        curr_hurtlex_lang=hurtlex_file[curr_lang]
        num_tokens.append(len(curr_tokens))
        for token in curr_tokens:
            token_escaped=escape_word_h5py(token)
            if(token_escaped in curr_hurtlex_lang):
                hurtlex_matrix[idx]+=curr_hurtlex_lang[token_escaped.lower()]
    if(tfidf):
        hurtlex_matrix=transformer.transform(hurtlex_matrix).toarray()
    else:
        for i in range(len(hurtlex_matrix)):
            hurtlex_matrix[i,:]=hurtlex_matrix[i,:]/num_tokens[i]

    return hurtlex_matrix


def get_emotions(data):
    """ Get emotions features (for instagram only, for now)

        Args:
            data: the data to extract emotions from

        Returns:
            numpy array containing emotion confidence for each emotion
    """
    max_num_posts=0
    for post in data:
        max_num_posts=max(max_num_posts, len(post['emotion_comments']))

    emotions=np.full((len(data),max_num_posts,5),-1)
    for m_idx,media_session in enumerate(data):
        for p_idx,post_emotion in enumerate(media_session['emotion_comments']):
            emotions[m_idx,p_idx]=post_emotion

    return emotions

def get_sentiment_data(data):
    """Get an array of compound sentiment values for each tweet

    Args:
        data: a dataset with sentiment values pre-computed

    Returns:
        np array of sentiment values
    """
    sentiments=[[post['sentiment']] for post in data]
    return(np.asarray(sentiments))

def get_sentiment_posts(data):
    """Get an array of compound sentiment values for each post

    Args:
        data: a dataset with sentiment values pre-computed

    Returns:
        np array of sentiment values
    """
    max_num_posts=0
    for post in data:
        max_num_posts=max(max_num_posts, len(post['sentiment_comments']))

    sentiment_posts=np.full((len(data),max_num_posts,1),-1)
    for m_idx,media_session in enumerate(data):
        for p_idx,post_sentiment in enumerate(media_session['sentiment_comments']):
            sentiment_posts[m_idx,p_idx]=post_sentiment

    return sentiment_posts

def get_instagram_image_info(data, src_task, feature_type):
    if(feature_type=='prediction'):
        img_info=np.full((len(data),1), 0.5)
    else:
        img_info=np.zeros((len(data),2048))

    for i, media_session in enumerate(data):
        media_session_feature=media_session["img"][src_task][feature_type]
        if(media_session_feature is not None):
            img_info[i,:]=np.asarray(media_session_feature[:])

    return img_info

@lru_cache(maxsize=10000)
def spacy_pipeline(string,lang):
    if(lang not in nlp_multilingual):
        nlp_multilingual[lang]=spacy.load(lang, disable=['parser','ner','textcat'])
    nlp=nlp_multilingual[lang]
    return nlp(string)

@lru_cache(maxsize=10000)
def tokenize_string(string, lang):
    parsed_string=spacy_pipeline(string,lang)
    token_list=[token.text for token in parsed_string if token.text!='\ufe0f' ]

    return token_list

@lru_cache(maxsize=10000)
def lemmatize_string(string, lang):
    parsed_string=spacy_pipeline(string,lang)
    lemma_list=[token.lemma_ for token in parsed_string]

    return lemma_list

@lru_cache(maxsize=10000)
def pos_string(string,lang):
    parsed_string=spacy_pipeline(string,lang)

    pos_list=[token.pos_ for token in parsed_string]
    return pos_list
