import numpy as np

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from preprocess.text_normalization import normalize_string
from preprocess.feature_extraction import tokenize_string, lemmatize_string
from preprocess.feature_extraction import pos_string

def get_ngram_vectorizer(data, size_tuple, multiple_post_datapoint,
                         emojis, max_features=None):
    """Get the ngram vectorizer for the provided data

    Args:
        data: current data to fit the vectorizer
        size_tuple: size tuple for ngrams, in the format (min_num_tokens,
        max_num_tokens)

    Returns:
       tfidf vectorizer for ngrams
    """

    transcribe_emojis=(emojis=='emoji-transcription')

    if(multiple_post_datapoint):
        corpus=[]
        for media_session in data:
            curr_lang=media_session['language']
            media_session_normalized=[normalize_string(comment, curr_lang, normalize_hasthags, transcribe_emojis) for comment in media_session['comments']]
            lemma_media_session=nlp_multilingual([lemmatize_string(comment, curr_lang) for comment in media_session_normalized])
            for comment in lemma_media_session:
                corpus.append(" ".join([token.lemma_ for token in comment]))
    else:
        corpus=[]
        for post in data:
            curr_lang=post['language']
            doc=normalize_string(post['text'], curr_lang, transcribe_emojis)
            lemma_post=lemmatize_string(doc, curr_lang)
            lemma_post=" ".join(lemma_post)
            corpus.append(lemma_post)

    count_vectorizer = CountVectorizer(ngram_range=size_tuple,
                                       max_features=max_features)

    counted = count_vectorizer.fit_transform(corpus)

    if multiple_post_datapoint:
        counted = []
        for media_session in data:
            curr_lang=post['language']
            lemma_comments=[normalize_string(comment, curr_lang, transcribe_emojis) for comment in media_session['comments']]
            lemma_comments = [lemmatize_string(comment) for comment in lemma_comments]
            lemmas_mediasession=[]
            for comment in lemma_comments:
               lemmas_mediasession.append(" ".join([lemma for lemma in comment])) 
            curr_msess=count_vectorizer.transform(media_session['comments'])
            curr_msess=curr_msess.toarray()
            curr_array=np.sum(curr_msess, axis=0)
            counted.append(curr_array)

    tfidf_transformer=TfidfTransformer(use_idf=True)

    tfidf_transformer.fit(counted)

    return count_vectorizer,tfidf_transformer

def get_emolex_vectorizer(data, multiple_post_datapoint, emojis):
    import h5py
    from preprocess.text_normalization import escape_word_h5py

    emolex_file=h5py.File('../data/emotion-lexicon/lexicon.hdf5','r')

    emolex_matrix=np.zeros((len(data),10))

    transcribe_emojis=(emojis=='emoji-transcription')

    for idx, post in enumerate(data):
        curr_lang=post['language']
        curr_emolex_lang=emolex_file[curr_lang]
        doc=normalize_string(post['text'], curr_lang, transcribe_emojis)
        tokens=tokenize_string(doc, curr_lang)
        curr_emolex=np.zeros((10,))
        for token in tokens:
            token_escaped=escape_word_h5py(token)
            if(token_escaped in curr_emolex_lang):
                curr_emolex_values=curr_emolex_lang[token_escaped]
                emolex_matrix[idx]+=(curr_emolex_lang[token_escaped])

    tfidf_emolex=TfidfTransformer()
    tfidf_emolex.fit(emolex_matrix)

    emolex_file.close()

    return tfidf_emolex


def get_hurtlex_vectorizer(data, multiple_post_datapoint, emojis):
    import h5py
    from preprocess.text_normalization import escape_word_h5py

    hurtlex_file=h5py.File('../data/hurtlex/lexicon.hdf5','r')

    hurtlex_matrix=np.zeros((len(data),19))
    
    transcribe_emojis=(emojis=='emoji-transcription')

    for idx, post in enumerate(data):
        curr_lang=post['language']
        curr_hurtlex_lang=hurtlex_file[curr_lang]
        doc=normalize_string(post['text'], curr_lang, transcribe_emojis)
        tokens=tokenize_string(doc, curr_lang)
        curr_hurtlex=np.zeros((10,))
        for token in tokens:
            token_escaped=escape_word_h5py(token)
            if(token_escaped in curr_hurtlex_lang):
                curr_hurtlex_values=curr_hurtlex_lang[token_escaped]
                hurtlex_matrix[idx]+=(curr_hurtlex_lang[token_escaped])

    tfidf_hurtlex=TfidfTransformer()
    tfidf_hurtlex.fit(hurtlex_matrix)

    hurtlex_file.close()

    return tfidf_hurtlex


def get_pos_vectorizer(data, size_tuple, multiple_post_datapoint):
    """Get the POS tfidf vectorizer for the provided data
    
    Args:
        data: data to fit the vectorizer
        size_tuple: size tuple for ngrams in the format (min_num_tokens,
        max_num_tokens)

    Returns:
        tfidf vectorizer for pos ngrams
    """

    pos_tags = []
    if(multiple_post_datapoint):
        for media_session in data:
            curr_lang=media_session['language']
            tags=[]
            for comment in tagged_comments:
                tags_comment=[pos for pos in pos_string(comment,curr_lang)]
                curr_tags=" ".join(tags_comment)
                pos_tags.append(curr_tags)
    else:
        for tweet in data:
            curr_lang = tweet['language']
            tweet_tags = pos_string(tweet['text'], curr_lang)
            tweet_tags = " ".join(tweet_tags)
            pos_tags.append(tweet_tags)
    
    count_vectorizer = CountVectorizer(ngram_range=size_tuple)
    
    counted=count_vectorizer.fit_transform(pos_tags)

    if(multiple_post_datapoint):
        counted=[]
        for media_session in data:
            curr_lang=media_session['language']
            tags=[]
            for comment in tagged_comments:
                tags_comment=[pos for pos in pos_string(comment,curr_lang)]
                curr_tags=" ".join(tags_comment)
                pos_tags.append(curr_tags)
            curr_msess = (count_vectorizer.transform(tags).toarray())
            counted.append(curr_msess.sum(axis=0))

    tfidf_transformer=TfidfTransformer()

    tfidf_transformer.fit(counted)

    return count_vectorizer, tfidf_transformer
