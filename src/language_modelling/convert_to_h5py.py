#!/usr/bin/env python3

import h5py
import csv
import argparse

def convert_to_h5py(input_filename, output_filename, input_num_lines):

    input_file = open(input_filename,'r')
    h5py_file = h5py.File(output_filename,'w')
    str_type = h5py.special_dtype(vlen=str)
    
    tweets=h5py_file.create_dataset('tweets', (input_num_lines,), dtype=str_type)

    for line_num,line in enumerate(input_file):
        curr_tweet_h5py=tweets[line_num]
        tweets[line_num]=line

    h5py_file.close()

if(__name__=='__main__'):
   parser = argparse.ArgumentParser()
   parser.add_argument('infile', action='store', help='input_file')
   parser.add_argument('outfile', action='store', help='output_file')
   parser.add_argument('inputsize', action='store', help='size of input file (lines)')

   args=parser.parse_args()

   print(args)

   convert_to_h5py(args.infile, args.outfile, int(args.inputsize))
