#!/usr/bin/env python3
import spacy
import os
import sys
import numpy as np
import h5py
import csv
from collections import deque
import math
import pickle


from model.model_configuration import get_ngram_vectorizer
from model.models import LanguageModel
from resources.Embeddings import Embeddings
from preprocess.text_normalization import escape_word_h5py
from model.model_configuration import ModelConfiguration


def train_deep_network(dataset_name,  num_epochs=20, 
                       transfer_dataset_name=None):
    
    weights_dir='../nnet-weights/{}/'.format(dataset_name)

    model_conf=ModelConfiguration(dataset_name, ["word2vec"], [],
                                  [], 'nnet-tflearn', 0, 32, 
                                  0, 0, False,
                                  recurrent_layer_size=1150, 
                                  recurrent_layer_type='LSTM',
                                  word2vec_source='multilingual-pivot')
    
    model_conf_file=open(weights_dir+'best-model-conf','wb')

    pickle.dump(model_conf, model_conf_file)

    model_conf_file.close()

    if(transfer_dataset_name is None):
        data_dir='../data/{}/'.format(dataset_name)
        retrain=False
    else:
        print('reatraining')
        data_dir='../data/{}/'.format(transfer_dataset_name)
        retrain=True

    print(data_dir)

    if(not os.path.isdir(weights_dir)):
        os.makedirs(weights_dir)
    
    if(not os.path.isfile(data_dir+'dict.hdf5')):
        h5py_embeddings=h5py.File('../data/embeddings/multilingual-pivot/embeddings.hdf5','r')
        embeddings={}
        embeddings['it']=Embeddings.from_h5py_file(h5py_embeddings,'it')
        embeddings['en']=Embeddings.from_h5py_file(h5py_embeddings,'en')
        
        vocabulary=set()
        i=0
        print('populate set from embeddings dict')
        for word in embeddings['en'].embeddings.keys():
            vocabulary.add(word.lower())
        
        h5py_embeddings.close()
        
        print('read vocabulary from file')
        try:
            h5py_vocabulary=h5py.File(data_dir+'vocabulary.hdf5','r')
        except:
            print('please use ./get_dict_from_h5pyfile.py on a dataset to '\
                  'create a dict h5py file from a dataset')
            return
        for word in h5py_vocabulary['vocabulary'].keys():
            if(word in vocabulary):
                frequency_count[word]=h5py_vocabulary['vocabulary'][word]

        vocabulary=None

        print('get top 10k words')
        dict_maxlen=min(len(frequency_count),10000)
        top_words_dict=[('',0) for i in range(dict_maxlen)]
        debug_idx=0
        for word in frequency_count:
            idx=0
            while(idx<len(top_words_dict)):
                if(frequency_count[word][0]<top_words_dict[idx][1]):
                    break
                else:
                    idx+=1

            if(idx!=0):
                top_words_dict.insert(idx,(word,frequency_count[word]))
                
            debug_idx+=1
            print('did {} of {}'.format(debug_idx, len(frequency_count)))

            top_words_dict=top_words_dict[-dict_maxlen:]
    
        print('get word2int dict')
        vocabulary={}
        for word_idx in range(len(top_words_dict)):
            word=top_words_dict[word_idx][0]
            vocabulary[word]=[word_idx]

        print('store top words to disk')
        if(not os.path.isdir(data_dir)):
            os.makedirs(data_dir)

        h5py_word2int=h5py.File(data_dir+'dict.hdf5')
        word2int_group=h5py_word2int.create_group('word2int')
        for word_raw in vocabulary:
            if(word_raw==''):
                continue
            word=escape_word_h5py(word_raw)
            word2int_group[word]=vocabulary[word]
    else:
        h5py_word2int=h5py.File(data_dir+'dict.hdf5')
        word2int_group=h5py_word2int['word2int']
        vocabulary={}
        for word in word2int_group:
            vocabulary[word]=word2int_group[word][0]

    h5py_word2int.close()
    
    save_callback=ModelCheckpoint('../nnet-weights/weights-model-best', 
                                  save_weights_only=True,
                                  save_best_only=True)

    input_filename=data_dir+'dataset.hdf5'
    input_file=h5py.File(input_filename,'r')
    dataset_size=input_file['tweets'].shape[0]
    input_file.close()

    model=LanguageModel(dataset_name, 300, len(vocabulary))

    if(retrain):
        model.load_model('../nnet-weights/{}/weights-model-best'.format(transfer_dataset_name),
                         weights_only=True)
    
    model.fit_generator(input_filename, dataset_size, vocabulary,
                        epochs=num_epochs)


