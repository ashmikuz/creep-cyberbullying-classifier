#!/usr/bin/env python3

import argparse
import sys
import h5py
import os
from collections import Counter
from multiprocessing import Pool
from operator import add
from functools import reduce
import spacy
import math


def get_word_set(parameter_tuple):
    from preprocess.text_normalization import normalize_string
    input_filename, line_num=parameter_tuple
    input_file = h5py.File(input_filename,'r')
    line=input_file['tweets'][line_num]
    try:
        words=normalize_string(line,'en',True)
    except:
        return None
    return words

def normalize_text(input_filename, output_filename):
    inputfile=h5py.File(input_filename, 'r')
    input_num_lines=inputfile['tweets'].shape[0]
    inputfile.close()
    pool=Pool()
    
    
    curr_batch=[(input_filename, i) for i in range(0, input_num_lines)]
    out_file=open(output_filename, 'w')
    
    total_lines=0
    for curr_count in pool.imap_unordered(get_word_set, curr_batch):
        if(curr_count is None):
            continue
        if(total_lines%1000==0):
            print('done {} lines'.format(total_lines))
        print(curr_count, file=out_file)
        total_lines+=1

    out_file.close()
    
if(__name__=='__main__'):
    os.chdir('..')
    sys.path.append('.')
    
    parser=argparse.ArgumentParser(description='get frequency of words from dataset')
    
    parser.add_argument('inputfile', action='store', help='input filename') 
    parser.add_argument('outputfile', action='store', help='output filename') 
                                                            
    args=parser.parse_args()

    normalize_text(args.inputfile, args.outputfile)
