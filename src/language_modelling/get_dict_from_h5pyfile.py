#!/usr/bin/env python3

import argparse
import sys
import h5py
import os
from collections import Counter
from multiprocessing import Pool
from operator import add
from functools import reduce
import spacy
import math

nlp=spacy.load('en',disable=['tagger','parser','ner','textcat'])

def get_word_set(parameter_tuple):
    from preprocess.text_normalization import normalize_string
    input_filename, line_num=parameter_tuple
    input_file = h5py.File(input_filename,'r')
    line=input_file['tweets'][line_num]
    words=normalize_string(line,'en',True)
    words=([word.text.lower() for word in nlp(words)])
    line_word_counter=Counter(words)
    input_file.close()
    return line_word_counter

def reduce_func(counters):
    if(len(counters)>0):
        return reduce(add, counters)
    else:
        return Counter()

def get_dictionary(input_filename, output_filename):
    from preprocess.text_normalization import normalize_string,escape_word_h5py
    
    inputfile=h5py.File(input_filename, 'r')
    input_num_lines=inputfile['tweets'].shape[0]
    inputfile.close()
    pool=Pool()
    
    word_counter=Counter()
    
    curr_batch=[(input_filename, i) for i in range(0, input_num_lines)]
    
    total_lines=0
    for curr_count in pool.imap_unordered(get_word_set, curr_batch):
        if(total_lines%1000 == 0):
            print('done {} lines'.format(total_lines))
        word_counter += curr_count
        total_lines+=1
    
    output_dict=h5py.File(output_filename,'w')
    vocabulary_group=output_dict.create_group('vocabulary')
    for word_raw in word_counter:
        word=escape_word_h5py(word_raw)
        vocabulary_group[word]=word_counter[word_raw]
    output_dict.close()

if(__name__=='__main__'):
    os.chdir('..')
    sys.path.append('.')
    
    parser=argparse.ArgumentParser(description='get frequency of words from dataset')
    
    parser.add_argument('inputfile', action='store', help='input filename') 
    parser.add_argument('outputfile', action='store', help='output filename') 
                                                            
    args=parser.parse_args()

    get_dictionary(args.inputfile, args.outputfile)
