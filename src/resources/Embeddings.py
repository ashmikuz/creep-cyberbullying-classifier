#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import h5py
import numpy as np
import re
import sys
from preprocess.text_normalization import escape_word_h5py
from functools import lru_cache

dict_filename='../data/words'

class Embeddings:
    """A wrapper class for word embeddings"""
    def __init__(self, h5py_file, vector_file_path=False, lang=None):
        """Inits the word embeddings wrapper (don't use this, better to use
        methods from_h5py_file and from_vectorfile)

        Args:
            h5py_file: the h5py file to save/load embeddings
            vector_file_path: the binary word2vec vector file to read 
                              embeddings from (optional if h5py_file is already
                              populated)
            lang: the language of the desired embeddings (needed for 
                  multilingual embeddings)
        """
        if(vector_file_path):
            self.populate_from_vectorfile(h5py_file, vector_file_path)
        else:
            self.populate_from_h5file(h5py_file, lang)
    
    @classmethod
    def from_h5py_file(cls, h5py_file, lang=None):
        """Get word embeddings from h5py file
        Args:
            h5py_file: the h5py file containing the embeddings
        
        Returns:
            the word embedding wrapper"""
        return cls(h5py_file, lang=lang)
    
    @classmethod
    def from_vectorfile(cls,h5py_file, vectorfile):
        """Get word embeddings from h5py file
        Args:
            h5py_file: the h5py file containing the embeddings
        
        Returns:
            the word embedding wrapper"""
        return cls(h5py_file, vectorfile)

    def normalize_vector(self,vector):
        """Normalize a vector (divide it by its norm)

        Args:
            vector: a word vector

        Returns:
            the normalized word vector
        """
        l2 = np.linalg.norm(vector)
        if(l2==0):
            l2=1
        return vector / l2
    
    @lru_cache(maxsize=1000000)
    def get_word_vector(self, word_raw, normalize=False):
        """Get a word embedding from a word

        Args:
            word: a word
            normalize: whether to normalize the vector (divide it by its norm)

        Return:
            a word embedding
        """
        
        word=escape_word_h5py(word_raw)

        if(word in self):
            embedding = self.embeddings[word]
        elif(('@' == word[0] or '#' == word[0])
                and word[1:] in self):
            embedding = self.embeddings[word[1:]]
        elif(('@' == word[0] or '#' == word[0])
                and word[1:].lower() in self):
            embedding = self.embeddings[word[1:].lower()]
        elif(word.lower() in self):
            embedding = self.embeddings[word.lower()]
        else:
            embedding = np.zeros((self.embedding_size,))

        if(normalize):
            return self.normalize_vector(embedding)
        else:
            return embedding

    def get_embeddings_stats(self):
        """Prints mean and std stats from the embeddings"""
        embeddings=np.zeros((len(self.embeddings),self.embedding_size))
        norms=np.zeros((len(self.embeddings),))
        n_word=0
        for _,val in self.embeddings.items():
            v=self.normalize_vector(np.array(val))
            norms[n_word]=np.linalg.norm(v,2)
            embeddings[n_word]=v
            n_word+=1
        mean=np.mean(embeddings)
        std=np.std(embeddings)
        print("mean is {}, std is {}".format(mean,std))

    def __contains__(self, word):
        """Checks if a word is in the embeddings

        Args:
            word: the word to check

        Returns:
            whether the word is in the embeddings
        """
        return escape_word_h5py(word) in self.embeddings

    def populate_from_vectorfile(self,h5py_file, vector_file_path):
        """Populate the h5py file from the binary word2vec file

        Args:
            h5py_file: the h5py file to create
            vector_file_path: the path for the binary word2vec file
        """
        import sys
        self.embeddings=h5py_file.create_group('embeddings')
        f=open(vector_file_path, 'rb')
        header=f.readline().split()
        
        vocab_size=int(header[0])
        vector_size=int(header[1])

        max_v=np.finfo(np.float32).min
        min_v=np.finfo(np.float32).max
        for i in range(vocab_size):
            sys.stdout.write('\rreading vector {} of {}'.format(i,vocab_size))
            word=b''
            while True:
                c=f.read(1)
                if c==b' ':
                    break
                word+=c
            word=word.decode('ISO-8859-1')
            vector=[]
            c=f.read(np.dtype(np.float32).itemsize * vector_size)
            vector=np.fromstring(c,dtype=np.float32)
            word=escape_word_h5py(word)
            self.embeddings.create_dataset(word, (len(vector),), data=vector)
            for v in vector:
                if(v>max_v):
                    max_v=v
                if(v<min_v):
                    min_v=v
        sys.stdout.write('\n')
        sys.stdout.flush()
    
    def populate_from_h5file(self, h5py_file, lang):
        """Populate embeddings from the h5py file
        
        Args:
            h5py_file: the h5py file containing embeddings
            lang: the language (optional, needed for multilingual embeddings
        """
        if(lang):
            self.embeddings=h5py_file[lang]
        else:
            self.embeddings=h5py_file['embeddings']
        sample_key=list(self.embeddings.keys())[0]
        self.embedding_size=self.embeddings[sample_key].shape[0]

    def get_min_distance_word(self, word, pool):
        distancefun=LevenstheinDistance(word)
        dist_words=pool.map(distancefun, self.embeddings.keys())

        distances=[dist[0] for dist in dist_words]

        amin_dist=np.argmin(distances)

        min_dist=distances[amin_dist]
        min_word=dist_words[amin_dist][1]
        
        return min_dist, min_word


class LevenstheinDistance(object):
    def __init__(self, source):
        self.source=source

    def __call__(self, target):
        #normalize ruspaa
        last_letter=self.source[-1]
        idx=-1
        while(len(self.source)>=abs(idx-1) and self.source[idx-1]==last_letter):
            idx-=1

        if(idx<-3):
            self.source=self.source[:idx+2]

        source_len=len(self.source)
        target_len=len(target)
        distance_threshold=min(source_len,2)
        if(abs(source_len-target_len)>distance_threshold):
            return np.Inf, target

        distance_matrix=np.zeros((source_len+1, target_len+1))
        
        distance_matrix[:,0]=range(0,source_len+1)
        
        distance_matrix[0,:]=range(0,target_len+1)

        for i in range(1,source_len+1):
            for j in range(1, target_len+1):
                if(self.source[i-1]==target[j-1]):
                    sub_cost=0.
                elif(self.source[i-1].lower()==target[j-1].lower()):
                    sub_cost=0.1
                else:
                    sub_cost=1.
                
                distance_matrix[i,j] = np.min([distance_matrix[i-1,j]+1,
                                               distance_matrix[i,j-1]+1,
                                               distance_matrix[i-1,j-1]+sub_cost])
        return distance_matrix[source_len, target_len], target


class FastTextEmbeddings(Embeddings):
    def __init__(self, model_name):
        import fasttext
        self.model=fasttext.load_model(model_name)

    def get_word_vector(self, word, normalize_vector=False):
        word_vector = self.model.get_word_vector(word)
        if(normalize_vector):
            return self.normalize_vector(word_vector)
        else:
            return word_vector

    def __contains__(self, word):
        return True

    def __del__(self):
        del(self.model)


# You can run the script directly to convert a binary word2vec file to a
# h5py file
if __name__=='__main__':
    if(len(sys.argv)<=3):
        print('usage: ./Embeddings.py word2vec_file hdf5_output')

    f=h5py.File(sys.argv[2] ,'w')
    v=Embeddings.from_vectorfile(f, sys.argv[1])
