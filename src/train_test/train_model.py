#!/usr/bin/env python3

import os
import numpy as np

from keras.models import load_model

from model.models import ModelClassifier
from train_test.sequences import TrainSequence, TestSequence
from model.model_configuration import configuration_generator 
from preprocess.feature_extraction import get_social_features, get_all_features
from preprocess.feature_extraction import get_feature_means_stds
from train_test.sequences import restore_rng

def train_network(x_train, y_train, training_epochs, model_conf, noresume):
    """Train a single model (from a single ModelConfiguration)
    
    Args:
        x_train: training data (no features extracted)
        y_train: training_labels
        training_epochs: number of training epochs
        model_conf: current ModelConfiguration
        noresume: don't resume training from saved files
    """

    directory_name=model_conf.get_directory_name()
    initial_epoch=0

    #get additional data for the required features (fit vectorizer, means,...)
    model_conf.fit_all_vectorizers(x_train)
    
    #handle resuming training
    if(not noresume):
        for i in range(1,training_epochs+1):
            filename=model_conf.get_file_name(i)
            if(os.path.isfile(filename)):
                initial_epoch=i
        
        if(initial_epoch==training_epochs):
            print('model already trained for {} epochs, skipping'\
                  .format(initial_epoch))
            return

        elif(initial_epoch>0):
            print('model already trained for {} of the {} total epochs'\
                  .format(initial_epoch, training_epochs))
            filename=model_conf.get_file_name(initial_epoch)
            model=ModelClassifier.from_file(model_conf, y_train.shape[1],filename)

    
    #handle the svm case, where the possible binary labels are [-1,1]
    if(model_conf.model_type=='svm' and y_train.shape[1]==1):
        train_labels=np.where(y_train==1, 1, -1)
    else:
        train_labels=y_train

    if(initial_epoch==0):
        model = ModelClassifier(model_conf, train_labels.shape[1])

        if (not os.path.isdir(directory_name)):
            os.makedirs(directory_name, exist_ok=True)
    
    # the number of batches is the int division between number of training
    # examples and batch size
    batch_size=model_conf.batch_size

    #turns out using a generator is actually faster for RNN models (sometimes)
    use_generator=('char-rnn' in model_conf.text_features or
                   'insta2vec' in model_conf.text_features)

    use_generator=False

    #train without generator
    try:
        if(not use_generator):
            train_vector=get_all_features(x_train, model_conf) 
            restore_rng(initial_epoch, False, len(x_train))
            model.fit(train_vector, train_labels, training_epochs, 
                      initial_epoch)

    except MemoryError as error:
        print('could not fit the entire dataset in memory, will use a '
              'generator (sequence) instead')
        use_generator=True


    #train with generator 
    if(use_generator):
        model.fit_generator(x_train, train_labels, epochs=training_epochs,
                            initial_epoch=initial_epoch)

    #destroy model, clear tf session
    del(model)
    

    return

def all_parameters_training(x_train, y_train, config_filename,
                            num_epochs, noresume):
    """Train on all possible combinations of parameters.
    
    Args:
        x_train: training data (no features extracted yet)
        y_train: training labels
        config_filename: name of the model configuration file
        num_epochs: number of epochs for training
        noresume: boolean, whether to resume the training from saved models
    """
    try:
        for model_conf in configuration_generator(config_filename):
            print('training {}'.format(model_conf))
            train_network(x_train, y_train, num_epochs, model_conf, noresume)

    except StopIteration:
        return

