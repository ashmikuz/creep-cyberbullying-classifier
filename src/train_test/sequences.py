#!/usr/bin/env python3

import numpy as np
from keras.utils import Sequence
import math
import random
import h5py
import spacy

from resources.Embeddings import Embeddings
from preprocess.feature_extraction import get_sentiment_data, get_ngrams
from preprocess.feature_extraction import get_social_features, get_cbows_sum_avg
from preprocess.feature_extraction import get_pos, get_feature_means_stds
from preprocess.feature_extraction import get_all_features
from preprocess.vectorizers import get_ngram_vectorizer
from preprocess.twokenize import tokenizeRawTweetText

class TrainSequence(Sequence):
    """Sequence class to train a model, inherits from keras.utils.Sequence"""
    def __init__(self, x_train, y_train, model_conf, initial_epoch=1):
        """Inits TrainSequence

        Args:
            x_train: training data (no feature extraction done)
            y_train: training labels
            model_conf: a ModelConfiguration object
            initial_epoch: initial epoch for training
        """

        self.x=x_train
        self.y=y_train
        self.model_conf=model_conf
        self.batch_size=model_conf.batch_size
        self.sample_num=len(x_train) // self.batch_size

        restore_rng(initial_epoch, True)

    def __len__(self):
        """Get the length of the sequence (size=number_samples//batch_size)
        
        Returns:
            the length of the sequence"""
        return self.sample_num

    def __getitem__(self, idx):
        """Get the idx-th batch for training

        Args:
            idx: the idx we want training data of

        Returns:
            a tuple (training_features, training_labels)
        """
        batch_x = self.x[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_y = self.y[idx * self.batch_size:(idx + 1) * self.batch_size]
        
        features=get_all_features(batch_x, self.model_conf)

        return (features, batch_y)

    def on_epoch_end(self):
        """This function gets called after every epoch, it shuffles the
        data
        """
        permutation=np.random.permutation(len(self.x))
        self.x=self.x[permutation]
        self.y=self.y[permutation]
        return


class TestSequence(Sequence):
    """Sequence class to test a model, inherits from keras.utils.Sequence"""
    def __init__(self, x_test, model_conf):
        self.x=x_test
        self.model_conf=model_conf
        self.batch_size=model_conf.batch_size
        self.sample_num=math.ceil(len(x_test) / self.batch_size)

    def __len__(self):
        """Get the length of the sequence (size=number_samples//batch_size)
        
        Returns: length of the sequence"""
        return self.sample_num

    def __getitem__(self, idx):
        batch_x = self.x[idx * self.batch_size:(idx + 1) * self.batch_size]

        features=get_all_features(batch_x, self.model_conf)

        return (features)


class TrainLanguageModelSequence(Sequence):
    """Sequence class to train a model, inherits from keras.utils.Sequence"""
    def __init__(self, h5py_filename, model_conf, vocabulary, batch_size, 
                 indices_tuple, initial_epoch=0):
        """Inits TrainSequence

        Args:
            x_train: training data (no feature extraction done)
            y_train: training labels
            model_conf: a ModelConfiguration object
            initial_epoch: initial epoch for training
        """

        self.h5py_filename=h5py_filename
        self.model_conf=model_conf
        self.batch_size=batch_size
        self.sample_num=(indices_tuple[1]-indices_tuple[0]) // batch_size
        self.start_idx=indices_tuple[0]
        self.end_idx=indices_tuple[1]

        self.word_dict=vocabulary
        self.last_idx=0
        self.nlp=spacy.load('en',disable=['tagger','parser','ner','textcat'])

        restore_rng(initial_epoch, True)

    def __len__(self):
        """Get the length of the sequence (size=number_samples//batch_size)
        
        Returns:
            the length of the sequence"""
        return self.sample_num

    def __getitem__(self, idx):
        """Get the idx-th batch for training

        Args:
            idx: the idx we want training data of

        Returns:
            a tuple (training_features, training_labels)
        """
        
        dataset_h5py_file=h5py.File(self.h5py_filename,'r')
        
        batch_x = dataset_h5py_file['tweets'][self.start_idx + idx * self.batch_size:(self.start_idx+idx + 1) * self.batch_size]
        
        self.last_idx=idx

        #FIXME
        max_num_words=150
        embedding_size=300

        x_train=np.full((len(batch_x), max_num_words, embedding_size),-1)
        y_train=np.zeros((len(batch_x), max_num_words, len(self.word_dict)))
        tweet_idx=0

        x_train=get_all_features(batch_x, model_conf)
                
        for tweet in batch_x:
            tokenized_tweet=[token.text for token in self.nlp(tweet)]
            word_idx=0
            for word in tokenized_tweet:
                if(word in self.word_dict):
                    word_categorical=self.word_dict[word]
                    y_train[tweet_idx, word_idx,word_categorical] = 1.
                    word_idx+=1
            if(word_idx>1):
                tweet_idx+=1

        x_train = x_train[:,:-1,:]
        y_train = y_train[:,1:,:]

        vec_h5py_file.close()
        dataset_h5py_file.close() 
        
        return (x_train, y_train)

def restore_rng(initial_epoch, use_generator=True, num_training_samples=0,
                batch_size=10):
    """ make the experiment reproducible by cycling through random generator
    states
    """
    if(use_generator):
        np.random.seed(42)
        for i in range(1,initial_epoch):
            np.random.permutation(num_training_samples)
        
        random.seed(42)
        for i in range(1, initial_epoch):
            random.shuffle(range(num_training_samples // batch_size))
    else:
        np.random.seed(42)
        np.random.shuffle(np.arange(num_training_samples))


