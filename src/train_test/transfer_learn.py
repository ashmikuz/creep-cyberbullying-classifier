#!/usr/bin/env python3
from keras.models import load_model
from preprocess.feature_extraction import get_all_features
from preprocess.data_import import get_label_names
from model.models import LanguageModel,ModelClassifier
from metrics.performance_metrics import get_performance_metrics
from keras.callbacks import Callback, ModelCheckpoint
import keras.backend as K
import numpy as np
import pickle
import math
import os

def transfer_train(x_train, y_train, x_val, y_val, model_conf, training_epochs, 
                   transfer_train_folder, transfer_type):

    
    if(transfer_type == 'task'):
        model=LanguageModel(model_conf.dataset_name, 300, 1)
        model.load_model(transfer_train_folder+'weights-model-best', 
                         weights_only=True)
    else:
        model=ModelClassifier.from_file(model_conf ,transfer_train_folder+'weights-model-best')

    models_savefolder=model_conf.get_directory_name()
    os.makedirs(models_savefolder, exist_ok=True)
    
    
    unfreeze_strategy='bottom_up'

    if(unfreeze_strategy=='gradual'):
        train_gradual_unfreezing(x_train,y_train, model, model_conf, 
                                 training_epochs)
    elif(unfreeze_strategy=='bottom_up'):
        train_bottom_up(x_train, y_train, x_val, y_val,model, model_conf, 
                        training_epochs)



def train_gradual_unfreezing(x_train, y_train, model, model_conf,
                             training_epochs):


    batch_size=model_conf.batch_size
    batch_per_epoch=len(x_train) // batch_size

    callback_lrtriang=SlantedTriangularLearning(training_epochs, batch_per_epoch)

    model.populate_callbacks([callback_lrtriang])

    train_vector=get_all_features(x_train, model_conf) 

    #handle the svm case, where the possible binary labels are [-1,1]
    if(model_conf.model_type=='svm' and y_train.shape[1]==1):
        labels_train=np.where(y_train==1, 1, -1)
    else:
        labels_train=y_train

    for epoch in range(training_epochs):
        #enable training on layers
        model.disable_layers(epoch)
        
        #train model
        model.fit(train_vector, labels_train, 
                  initial_epoch=epoch, 
                  epochs=epoch+1)
        
        #save
        model_filename=model_conf.get_file_name(epoch+1)
        model.model.save(model_filename)

        #shuffle data!
        shuffle=np.random.permutation(len(x_train))
        train_vector=[feature[shuffle] for feature in train_vector]
        labels_train=labels_train[shuffle]
    
    del(model)


def train_bottom_up(x_train, y_train, x_val, y_val, model, model_conf, training_epochs):

    train_vector=get_all_features(x_train,model_conf)
    val_vectors=get_all_features(x_val, model_conf)

    #handle the svm case, where the possible binary labels are [-1,1]
    if(model_conf.model_type=='svm' and y_train.shape[1]==1):
        labels_train=np.where(y_train==1, 1, -1)
        labels_val=np.where(y_val==1, 1, -1)
    else:
        labels_train=y_train
        labels_val=y_val


    fields=get_label_names(model_conf.dataset_name)
    num_layers=model.count_layers()
    print(num_layers)

    for layer_idx in range(num_layers):
        model_filename=model_conf.get_file_name()
        if(layer_idx < num_layers-1):
            model_filename+='enabled_layer_{}'.format(layer_idx)

        model_checkpoint=ModelCheckpoint(model_filename)
        model.populate_callbacks([model_checkpoint])

        model.enable_single_layer(layer_idx)

        model.fit(train_vector, labels_train, initial_epoch=0, epochs=training_epochs)
    
        best_fscore=0
        best_model_epoch=0
        del(model)
        for epoch in range(1, training_epochs+1):
            curr_model_filename=model_filename.format(epoch=epoch)
            print('testing {}'.format(curr_model_filename))
            model=ModelClassifier.from_file(model_conf, curr_model_filename)

            predictions = model.predict(val_vectors, batch_size=32) 

            performance, confusion=get_performance_metrics(fields, predictions,
                                                          model_conf, y_val)
            if(performance[-2, 2] >= best_fscore):
                best_fscore = performance[-2,2]
                best_model_epoch = epoch
        
            del(model)
        model=ModelClassifier.from_file(model_conf, model_filename.format(epoch=best_model_epoch))


class SlantedTriangularLearning(Callback):
    
    def __init__(self, num_epochs, batch_per_epoch):
        self.lr=0.0001
        self.total_batch_count=0
        self.cut_frac=0.1
        self.cut_ratio=10
        self.ratio=32
        self.lrmax=0.01
        self.total_iterations=num_epochs*batch_per_epoch
    
    def update_lr_value(self):
        t=self.total_batch_count
        cut=math.floor(self.total_iterations*self.cut_frac)
        if(t<cut):
            p=t/cut
        else:
            p=1-((t - cut) / (cut * (self.cut_ratio - 1)))
        self.lr=self.lrmax*((1 + p * (self.ratio - 1)) / self.ratio)
        return self.lr

    def on_train_begin(self,logs=None):
        self.update_lr_value()
        K.set_value(self.model.optimizer.lr, self.lr)

    def on_batch_end(self, batch, logs={}):
        self.total_batch_count+=1
        self.update_lr_value()
        K.set_value(self.model.optimizer.lr, self.lr)




