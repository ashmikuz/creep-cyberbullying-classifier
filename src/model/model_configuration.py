#!/usr/bin/env python3

from preprocess.feature_extraction import get_social_features, get_feature_means_stds
from preprocess.vectorizers import get_ngram_vectorizer, get_pos_vectorizer
from preprocess.vectorizers import get_emolex_vectorizer, get_hurtlex_vectorizer
import numpy as np

#sizes for the embeddings
embedding_sizes={'google':300, 'twitter':400, 'multilingual':300, 'german':200,
                 'multilingual-ger':300, 'multilingual-pivot':300,
                 'fasttext-it':300, 'fasttext-de':300, 'fasttext-es':300,
                 'fasttext-twitter-it':300, 'fasttext-crawl':300,
                 'spinningbytes':300,'english_pivot':300,'fastext_news':300,'fastext_200':200}

class ModelConfiguration:
    """A configuration for a model

    It contains all the informations about hyperparameters and features
    """
    def __init__(self, train_dataset_name, test_dataset_name,text_features, other_features,
                 hidden_layers, model_type, regularization_lambda, batch_size,
                 dropout, recurrent_dropout, use_batch_normalization,
                 emojis, recurrent_layer_sizes=None,
                 recurrent_layer_type=None, word2vec_source=None,
                 attention=False):
        """Inits ModelConfiguration with the provided values

        Args:
            text_features: a list of  text-derived features (ngrams, embeddings,...);
            other_features: a list of other features (sentiment and social);
            hidden_layers: a list of sizes for hidden layers;
            model_type: specifies the model type, can be nnet,svm or lr;
            regularization_lambda: l2 regularization constant;
            batch_size: batch size for training;
            dropout: a list of dropout values or a single float in [0,1];
            recurrent_dropout: a single float in [0,1] specifying recurrent dropout;
            use_batch_normalization: whether to use batchnorm between hidden feed-forward layers;
            recurrent_layer_sizes: the sizes of the recurrent layers;
            recurrent_layer_type: the recurrent layer to use (LSTM, GRU or CNN+GRU);
            word2vec_source: the source for word embeddings, can be google, twitter or multilingual.
        """
        self.train_dataset_name = train_dataset_name
        self.test_dataset_name = test_dataset_name
        self.dataset_name = 'train-{}-test-{}'.format(train_dataset_name, test_dataset_name)
        self.text_features = text_features
        self.other_features = other_features
        self.hidden_layers = hidden_layers
        self.model_type = model_type
        self.regularization_lambda = regularization_lambda
        self.batch_size = batch_size
        self.attention=attention

        if(isinstance(dropout, list)):
            self.dropout = dropout[:len(self.hidden_layers)+int('word2vec' in text_features)]
        else:
            self.dropout = [dropout]*(len(self.hidden_layers)+int('word2vec' in text_features))

        self.recurrent_dropout=recurrent_dropout

        self.use_batch_normalization=use_batch_normalization
        self.emojis=emojis

        self.recurrent_layer_type=recurrent_layer_type
        self.word2vec_source=word2vec_source

        if(word2vec_source is not None or 'cbows' in self.text_features):
            self.embedding_size=embedding_sizes[word2vec_source]

        self.recurrent_layer_sizes=[]
        if(recurrent_layer_sizes):
            for size in recurrent_layer_sizes:
                if(size=='embeddings'):
                    self.recurrent_layer_sizes.append(self.embedding_size)
                else:
                    self.recurrent_layer_sizes.append(size)

    def get_directory_name(self, base_dir='../nnet-weights'):
        """Get the directory name for the current configuration

        Args:
            base_dir: the base directory (default=../nnet-weights)
        """
        dataset_dir_name='{}/{}/'.format(base_dir,self.dataset_name)
        vector_source_name=[]
        if(self.word2vec_source is not None):
            vector_source_name.append(self.word2vec_source+"_vectors")
        return dataset_dir_name+"-".join([self.model_type] + \
                                          self.text_features+ \
                                          self.other_features+ \
                                          vector_source_name)

    def fit_all_vectorizers(self, data):
        if(self.use_ngrams()):
            ngram_sizes = self.get_ngram_sizes()
            multiple_post_datapoint=self.dataset_name=='instagram-cyberbullying'
            self.ngram_vectorizer, self.ngram_transformer = get_ngram_vectorizer(data,
                                                                                 ngram_sizes,
                                                                                 multiple_post_datapoint,
                                                                                 self.emojis)

        if(self.use_pos()):
            pos_sizes = self.get_pos_sizes()
            multiple_post_datapoint=self.dataset_name=='instagram-cyberbullying'
            self.pos_vectorizer, self.pos_transformer = get_pos_vectorizer(data,
                                                                           pos_sizes,
                                                                           multiple_post_datapoint)


        if('emolex' in self.other_features):
            multiple_post_datapoint=(self.dataset_name=='instagram-cyberbullying')
            self.emolex_transformer = get_emolex_vectorizer(data,multiple_post_datapoint,
                                                            self.emojis)

        if('hurtlex' in self.other_features):
            multiple_post_datapoint=(self.dataset_name=='instagram-cyberbullying')
            self.hurtlex_transformer = get_hurtlex_vectorizer(data,multiple_post_datapoint,
                                                              self.emojis)

        if("social" in self.other_features):
            social_features=get_social_features(data)
            self.means,self.stds=get_feature_means_stds(social_features)

        if("char-rnn" in self.text_features or 
           "insta2vec" in self.text_features):

            import string
            self.char2int={}
            self.char_count=0
            self.max_num_chars=0
            self.max_num_comments=198

            for post in data:
                curr_post=post['text'].lower()
                if('insta2vec' in self.text_features):
                    for comment in post['comments']:
                        self.max_num_chars=max(self.max_num_chars,len(comment))
                else:
                    self.max_num_chars=max(self.max_num_chars, len(curr_post))
                for char in curr_post:
                    if(char not in self.char2int):
                        self.char2int[char]=self.char_count+1
                        self.char_count+=1


    def use_pos(self):
        """Whether the model uses POS features

        Returns:
            True if pos features are used, False otherwise
        """

        return 'pos_unigrams' in self.text_features or \
               'pos_bigrams' in self.text_features

    def use_ngrams(self):
        """Wherther the model uses ngram features

        Returns:
            True if ngrams features are used, False otherwise
        """
        return 'unigrams' in self.text_features or \
               'bigrams' in self.text_features


    def get_ngram_sizes(self):
        """Get the appropriate tuple for the specified ngram features

        Returns:
            A tuple of the form (min_num_tokens,max_num_tokens) that
            specifies the ngram sizes, None if no ngrams are used
        """
        if('unigrams' in self.text_features and 
           'bigrams' in self.text_features):
            sizes=(1,2)
        elif('unigrams' in self.text_features):
            sizes = (1,1)
        elif('bigrams' in self.text_features):
            sizes = (2,2)
        else:
            sizes = None
        return sizes

    def get_pos_sizes(self):
        """Get the appropriate tuple for the specified POS features

        Returns:
            A tuple of the form (min_num_tokens,max_num_tokens) that
            specifies the POS ngrams sizes, None if no ngrams are used
        """
        if('pos_unigrams' in self.text_features and 
           'pos_bigrams' in self.text_features):
            pos_sizes = (1,2)
        elif('pos_unigrams' in self.text_features):
            pos_sizes = (1,1)
        elif('pos_bigrams' in self.text_features):
            pos_sizes = (2,2)
        else:
            pos_sizes = None
        return pos_sizes

    def __get_batch_norm_string(self):
        """Get the batch normalization string(for the filename)

        Returns:
            the batchnorm string for model filename
        """
        if(self.use_batch_normalization):
            return "-batchnorm"
        else:
            return ""

    def __get_emoji_str(self):
        if(self.emojis):
            return "-"+self.emojis
        else:
            return ""


    def get_file_name(self, epoch = None, for_weights=True):
        """Get the filename for this model

        Args:
            epoch: epoch number (optional, if not provided the returned
                   string will need to be formatted later with the epoch num

        Returns:
            filename for this model
        """
        directory_name = self.get_directory_name()
        if(for_weights):
            basename='weights'
        else:
            basename='config'
        if(self.model_type=='nnet'):
            filename = '{}/{}-{}layers{}-alpha{}-batch{}-dropout{}{}{}'\
                                          .format(directory_name, basename,
                                          len(self.hidden_layers),
                                          self.hidden_layers[0],
                                          self.regularization_lambda,
                                          self.batch_size,
                                          self.dropout,
                                          self.__get_emoji_str(),
                                          self.__get_batch_norm_string())

            if('word2vec' in self.text_features):
                filename+='-size{}'.format(self.recurrent_layer_sizes)
                filename+='-{}'.format(self.recurrent_layer_type)
                filename+='-recdropout{}'.format(self.recurrent_dropout)
                if(self.attention):
                    filename+='-attention'

            filename += '-epoch{epoch}'

        else:
            filename = '{}/{}-alpha{}-batch{}'\
                                          .format(directory_name, 
                                                  basename,
                                                  self.regularization_lambda,
                                                  self.batch_size)
            filename += '-epoch{epoch}'

        if(epoch is not None):
            filename=filename.format(epoch=epoch)

        return filename


    def __str__(self):
        """String representation of the class

        Returns:
            current filename (without epoch argument)
        """
        return self.get_file_name()


def configuration_generator(config_filename):
    """Generate possible configurations for the given dataset

    Args:
        config_filename: path for the config python file

    Yields:
        possible configurations
    """
    import itertools
    import copy
    import imp
    import json

    #load the config file (it's a json file)
    config_file=open(config_filename,'r')
    config=json.load(config_file)

    train_dataset_name=config['train_dataset']
    test_dataset_name=config['test_dataset']
    check_features_list(config)


    #nnet model configurations
    if('nnet' in config['all_model_types']):
        for feature_set in config['all_text_features']:
            #word2vec based configurations
            if('word2vec' in feature_set):
                for i in itertools.product(config['all_other_features'], 
                                           config['all_layer_sizes'],
                                           config['all_lambdas'], 
                                           config['all_batch_sizes'],
                                           config['all_dropouts'], 
                                           config['all_recurrent_dropouts'],
                                           config['all_use_batch_normalization'],
                                           config['all_emojis'],
                                           config['all_recurrent_layer_sizes'],
                                           config['all_recurrent_layer_types'],
                                           config['all_word2vec_sources'],
                                           config['all_attentions']):

                    other_features=i[0]
                    layer_size=i[1]
                    lambda_regularization=i[2]
                    batch_size=i[3]
                    dropout=i[4]
                    recurrent_dropout=i[5]
                    batch_normalization=i[6]
                    emojis=i[7]
                    recurrent_layer_size=i[8]
                    recurrent_layer_type=i[9]
                    word2vec_source=i[10]
                    attention=i[11]


                    yield ModelConfiguration(train_dataset_name, 
                                             test_dataset_name, feature_set,
                                             other_features, layer_size, 'nnet',
                                             lambda_regularization,
                                             batch_size, dropout,
                                             recurrent_dropout, 
                                             batch_normalization,
                                             emojis,
                                             recurrent_layer_size,
                                             recurrent_layer_type,
                                             word2vec_source,
                                             attention)

            elif('cbows' in feature_set):
                for i in itertools.product(config['all_other_features'], 
                                           config['all_layer_sizes'],
                                           config['all_lambdas'], 
                                           config['all_batch_sizes'],
                                           config['all_dropouts'], 
                                           config['all_use_batch_normalization'],
                                           config['all_emojis'],
                                           config['all_word2vec_sources']):

                    other_features = i[0]
                    layer_size = i[1]
                    lambda_regularization = i[2]
                    batch_size = i[3]
                    dropout=i[4]
                    use_batch_normalization = i[5]
                    emojis=i[6]
                    word2vec_source = i[7]

                    yield ModelConfiguration(train_dataset_name,
                                             test_dataset_name, feature_set,
                                             other_features, layer_size, 'nnet',
                                             lambda_regularization, batch_size,
                                             dropout, 0,
                                             use_batch_normalization,
                                             emojis,
                                             word2vec_source)

            #other configurations
            else:
                for i in itertools.product(config['all_other_features'], 
                                           config['all_layer_sizes'],
                                           config['all_lambdas'],
                                           config['all_batch_sizes'],
                                           config['all_dropouts'], 
                                           config['all_use_batch_normalization'],
                                           config['all_emojis']):

                    other_features=i[0]
                    layer_size=i[1]
                    lambda_regularization=i[2]
                    batch_size=i[3]
                    dropout=i[4]
                    batch_normalization=i[5]
                    emoji=i[6]

                    yield  ModelConfiguration(train_dataset_name,
                                              test_dataset_name, feature_set,
                                              other_features, layer_size, 'nnet',
                                              lambda_regularization, batch_size,
                                              dropout, 0, batch_normalization,
                                              emoji)

    all_text_features_svmlr=copy.deepcopy(config['all_text_features'])

    # for svms and logistic regression, dropout and word2vec embeddings make no 
    # sense
    for flist_idx in range(len(all_text_features_svmlr)):
        if('word2vec' in all_text_features_svmlr[flist_idx]):
            all_text_features_svmlr[flist_idx].remove('word2vec')
        if('char-rnn' in all_text_features_svmlr[flist_idx]):
            all_text_features_svmlr[flist_idx].remove('char-rnn')
        if('insta2vec' in all_text_features_svmlr[flist_idx]):
            all_text_features_svmlr[flist_idx].remove('insta2vec')

    #removed empty text feature sets
    flist_idx=0
    while flist_idx < (len(all_text_features_svmlr)):
        if(not all_text_features_svmlr[flist_idx]):
            all_text_features_svmlr.pop(flist_idx)
        else:
            flist_idx+=1

    #remove nnet model
    all_models_svmlr=config['all_model_types'][:]
    if('nnet' in all_models_svmlr):
        all_models_svmlr.remove('nnet')

    #svm and lr configurations
    for feature_set in all_text_features_svmlr:
        #word2vec based configurations
        if('cbows' in feature_set):
            for i in itertools.product(config['all_other_features'], 
                                       all_models_svmlr, 
                                       config['all_lambdas'], 
                                       config['all_batch_sizes'], 
                                       config['all_word2vec_sources'],
                                       config['all_normalize_hasthags'],
                                       config['all_emojis']):

                other_features=i[0]
                model_type=i[1]
                lambda_regularization=i[2]
                batch_size=i[3]
                word2vec_source=i[4]
                emojis=i[5]

                yield ModelConfiguration(train_dataset_name, test_dataset_name,
                                        feature_set, other_features, model_type,
                                        lambda_regularization, batch_size,
                                        0, 0, False, emojis)
        #other configurations
        else:
             for i in itertools.product(config['all_other_features'],
                                        all_models_svmlr, 
                                        config['all_lambdas'],
                                        config['all_batch_sizes'],
                                        config['all_emojis']):

                other_features=i[0]
                model_type=i[1]
                lambda_regularization=i[2]
                batch_size=i[3]
                emojis=i[4]

                yield ModelConfiguration(train_dataset_name, test_dataset_name,
                                         feature_set, other_features, [],
                                         model_type, lambda_regularization,
                                         batch_size, 0, 0, False,
                                         emojis)

    return


def check_features_list(config):
    """Check if the features specified in the global variables are valid
    Args:
        config: the config module

    Raises:
        ModelConfException: some features are not correctly specified in the
        global variables
    """
    import numpy as np

    #check text features
    all_possible_features_text = set(['pos_unigrams','pos_bigrams','unigrams',
                                      'bigrams', 'cbows', 'word2vec', 'char-rnn'])

    for text_features_possibility in config['all_text_features']:
        tf=set(text_features_possibility)
        valid_text_features = (tf <= all_possible_features_text)

        if(not valid_text_features):
            raise ModelConfException('invalid text feature(s) for the model')

    #check other features
    all_possible_other_features = set(['social', 'sentiment','emotion', 'emolex',
                                       'hurtlex', 'img_creender_predictions',
                                       'img_creender_descriptors',
                                       'img_sentiment_predictions',
                                       'img_sentiment_descriptors'])

    for other_features_possibility in config['all_other_features']:
        of=set(other_features_possibility)
        valid_other_features = (of <=  all_possible_other_features)

        if(not valid_other_features):
            raise ModelConfException('invalid other feature(s) for the model')

        img_features_found=False

        img_features_names=['img_crendeer_predictions', 
                            'img_creendeer_descriptors',
                            'img_sentiment_predictions',
                            'img_sentiment_descriptors']

        for name in img_features_names:
            if(name in other_features_possibility):
                img_features_found=True

        if(('emotion' in other_features_possibility or img_features_found)  and
            config['train_dataset']!='instagram-cyberbullying'):
            raise ModelConfException('emotion and image features can only be used for'\
                                     ' the instagram-cyberbullying dataset')

    #check word2vec sources
    all_possible_word2vec_sources=set(embedding_sizes.keys())

    if(config['all_word2vec_sources']):
        ws=set(config['all_word2vec_sources'])
        valid_word2vec_features= (ws<= all_possible_word2vec_sources) or None in ws
        if(not valid_word2vec_features):
            raise ModelConfException('invalid word2vec source')

    #check recurrent layer
    all_possible_recurrent_layers=set(['LSTM','BiLSTM', 'GRU','CNN+GRU'])

    rls = set(config['all_recurrent_layer_types'])
    valid_recurrent_layers = (rls<= all_possible_recurrent_layers)

    if(not valid_recurrent_layers):
        raise ModelConfException('invalid recurrent layer(s)')

    return

def get_number_configurations(config_filename):
    """Get the total number of configurations from the config file 

    Args:
        config_filename: path to the config python file

    Returns:
        the total number of configurations obtained from the global variables
    """
    import json

    config_file=open(config_filename,'r')
    config=json.load(config_file)

    n_iters=0
    other_types=config['all_model_types'][:]
    if('nnet' in other_types):
        use_word2vec=False
        for feature_set in config['all_text_features']:

            curr_n_iters = len(config['all_other_features'])*\
                       len(config['all_layer_sizes'])*len(config['all_lambdas'])*\
                       len(config['all_batch_sizes'])*len(config['all_dropouts'])*\
                       len(config['all_use_batch_normalization'])


            if('word2vec' in feature_set or 'cbows' in feature_set):
                curr_n_iters*=(len(config['all_recurrent_dropouts'])*\
                               len(config['all_word2vec_sources'])*\
                               len(config['all_recurrent_layer_types'])*\
                               len(config['all_recurrent_layer_sizes']))*\
                               len(config['all_attentions'])

            n_iters+=curr_n_iters

        other_types.remove('nnet')
    if(len(other_types)>0):
        for feature_set in config['all_text_features']:
            if('word2vec' in feature_set and len(feature_set)==1):
                continue
            if('cbows' in feature_set):
                 curr_n_iters= len(config['all_word2vec_sources'])
            else:
                curr_n_iters=1

            n_iters += len(other_types)*len(config['all_other_features'])*\
                       len(config['all_lambdas'])*len(config['all_batch_sizes']) * curr_n_iters

    return n_iters

def configuration_from_index(i, config_filename):
    """Get the i-th configuration

    Args:
        i: the configuration index

    Returns:
        the ModelConfiguration object for the i-th configuration
    """

    conf_generator = configuration_generator(config_filename)
    for i in range(i+1):
        model = next(conf_generator)
    return model

def get_dataset_names(config_filename):
    import json

    config_file=open(config_filename,'r')
    config=json.load(config_file)

    return config['train_dataset'], config['test_dataset']

def get_best_conf_filename(config_filename):
    import os
    generator=configuration_generator(config_filename)
    while True:
        curr_config=next(generator)
        config_pickle_filename=curr_config.get_file_name('best', False)
        if(os.path.isfile(config_pickle_filename)):
            return config_pickle_filename

class ModelConfException(Exception):
    """Raised when we have problems with the model configuration"""
    pass




