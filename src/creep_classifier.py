#!/usr/bin/env python

from preprocess.data_import import get_data, get_unlabeled_data
from preprocess.data_import import get_field_names, get_labels
from model.model_configuration import configuration_generator
from model.model_configuration import configuration_from_index
from model.model_configuration import get_dataset_names
from model.model_configuration import get_best_conf_filename
from metrics.performance_metrics import plot_confusion_matrix, plot_results
from metrics.performance_metrics import print_result_table
from train_test.train_model import all_parameters_training
from train_test.test_validate import test_validation_set, test_network, validate_from_single_config
from train_test.test_validate import label_unlabeled_file
from preprocess.feature_extraction import get_social_features, get_feature_means_stds
from shutil import copyfile
import pickle
import os
import json

def parse_arguments():
    """create a parser for the command line arguments using argparse.

    Returns:
        an argument parser
    """
    import argparse,sys

    parser=argparse.ArgumentParser(description='classifier for hate speech '\
                                                'and cyberbullying')

    parser.add_argument('--config_files', action='store', nargs='+',
                        help='specify the configuration file for the model',
                        default='../config/default_config.py')

    parser.add_argument('--train', action='store_true', 
                        help='train the network')

    parser.add_argument('--validate', action='store_true', 
                        help='use the validation set to choose the best '\
                             'performing network')
    parser.add_argument('--test', action='store_true', help='test the network')

    parser.add_argument('--label_file', action='store',
                        help='unlabeled file to label (see the text_field_no '\
                        'and the language_unlabeled file options)')

    parser.add_argument('--text_field_no', action='store', default=0,
                        help='specify the index of the text field in an '\
                        'unlabeled file')

    parser.add_argument('--language_unlabeled_file', action='store', 
                        default='en', help='specify the language used for the '\
                        'unlabeled file')

    parser.add_argument('--num_epochs', action='store', help='number of '\
                        'training epochs', default=10)

    parser.add_argument('--no_resume', action='store_true', help="don't resume"\
                        ' training from previously saved networks')

    parser.add_argument('--language_modelling', action='store_true',
                        help='train language modelling')

    return parser.parse_args()


def main(args):
    """ main functionality, reads command line options, datasets from file
    and performs the required operation(s) (train,validate,test)

    Args:
        args: command line arguments
    """

    num_epochs = int(args.num_epochs)

    language_modelling=args.language_modelling 

    if(not(args.train or args.validate or args.test or 
           args.label_file is not None)):
        train=True
        validate=True
        test=True
        rank=False
        label_file=None
    else:
        train=args.train
        validate=args.validate
        test=args.test
        label_file=args.label_file

    noresume=args.no_resume
    config_filenames=args.config_files

    for config_filename in config_filenames:

        result_filename=config_filename.split('/')[-1]
        if(os.path.isfile('../results/{}'.format(result_filename)) and
           os.path.getsize('../results/{}'.format(result_filename))>0 and
           not noresume):
            print('skipping train/validate/test for config {}, we have the results already'.format(config_filename))
            train=False
            validate=False
            test=False

        train_dataset_name, test_dataset_name = get_dataset_names(config_filename)
        dataset_name='train-{}-test-{}'.format(train_dataset_name, test_dataset_name)

        # get data and cateogories
        data = get_data(train_dataset_name, test_dataset_name)
        train_fields=get_field_names(train_dataset_name)
        test_fields= get_field_names(test_dataset_name)

        if(len(data)==2):
            x_train, test_data=data

            # validation data is half of the testing data (20% of complete dataset)
            validation_input_no = int(round(len(test_data)*0.5))

            validation_fields=test_fields
            test_labels=get_labels(test_data,validation_fields)

            validation_data = test_data[:validation_input_no]
            validation_labels = test_labels[:validation_input_no]

            test_data=test_data[validation_input_no:]
            test_labels=test_labels[validation_input_no:]
        else:
            x_train, validation_data, test_data=data

            validation_fields=train_fields

            validation_labels=get_labels(validation_data,train_fields)
            test_labels=get_labels(test_data, test_fields)

        y_train=get_labels(x_train, train_fields)


        #train from model_conf
        if(train):
            all_parameters_training(x_train, y_train, config_filename, num_epochs, noresume)

        # VALIDATION
        if(validate):
            model_idx, model_results = test_validation_set(validation_data,
                                                           validation_labels,
                                                           config_filename,
                                                           validation_fields,
                                                           num_epochs,
                                                           x_train)
            model_num, epoch_num=model_idx

            # plot metrics for all epochs of the best model
            plot_results(model_results[model_num], num_epochs,validation_fields, 
                         dataset_name)

            # #print best results
            print_result_table(model_results[model_idx], validation_fields)

            # keep the best result, so that we can replicate the test without
            # re-selecting the best network
            model_conf=configuration_from_index(model_num, config_filename)

            # fit auxiliary parameters to test set (tfidfvectorizer, means for 
            # social features, dictionary for char2vec approaches)
            model_conf.fit_all_vectorizers(x_train)

            # save the best model and configuration for the given dataset
            dataset_directory='../nnet-weights/{}/'.format(dataset_name)
            best_conf_filename=model_conf.get_file_name('best', False)
            with open(best_conf_filename,'wb') as conf_file:
                pickle.dump(model_conf, conf_file)

            best_weights_filename=model_conf.get_file_name('best', True)
            copyfile(model_conf.get_file_name(epoch_num+1),
                     best_weights_filename)

        # TESTING
        if(test):
            # load the best model
            best_conf_filename=get_best_conf_filename(config_filename)

            with open(best_conf_filename,'rb') as conf_file:
                model_conf=pickle.load(conf_file)

            # test the network
            result_matrix, confusion_matrix,predictions=test_network(test_data,
                                                                     test_labels,
                                                                     test_fields,
                                                                     model_conf)

            #save confusion matrix to file
            plot_confusion_matrix(confusion_matrix[0], test_fields, dataset_name)

            #print the results
            print_result_table(result_matrix[0],test_fields)

            #save a unique file (by conf name)
            results_as_list=[[result for result in line] for line in result_matrix[0]]
            result_filename=config_filename.split('/')[-1]
            result_file=open('../results/{}'.format(result_filename),'w')
            results_as_list=[model_conf.get_file_name(), results_as_list]
            json.dump(results_as_list, result_file)
            result_file.close()

            #save predictions as json
            predictions_file=open('../results/predictions_{}'.format(result_filename),'w')
            json.dump(predictions.tolist(), predictions_file)
            predictions_file.close()

        if(label_file):
            #read and parse the unlabeled file, using the specified text field number
            text_field_no=int(args.text_field_no)
            language_unlabeled_file=args.language_unlabeled_file
            test_data = get_unlabeled_data(label_file, text_field_no,
                                           language_unlabeled_file) 

            dataset_directory='../nnet-weights/{}/'.format(dataset_name)
            best_conf_filename=get_best_conf_filename(config_filename)
            #best_model_conf_filename = dataset_directory+'best-model-conf'+config_basename

            with open(best_conf_filename,'rb') as conf_file:
                model_conf=pickle.load(conf_file)

            model_conf.fit_all_vectorizers(x_train)

            label_unlabeled_file(label_file, model_conf, test_data, text_field_no,
                                 test_fields)


if __name__ == '__main__':
    args=parse_arguments()
    main(args)
