#!/bin/bash

echo "downloading twitter vector file"
wget "http://yuca.test.iminds.be:8900/fgodin/downloads/word2vec_twitter_model.tar.gz";
echo "extracting vector file"
tar xvzf "word2vec_twitter_model.tar.gz";

echo "converting vectors"
./convert_vectors.py

echo "free some space, remove the binary files"
rm word2vec_twitter_model/ word2vec_twitter_model.bin
