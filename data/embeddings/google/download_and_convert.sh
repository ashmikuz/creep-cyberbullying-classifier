#!/bin/bash

echo "downloading google vector file"
#wget  "https://s3.amazonaws.com/dl4j-distribution/GoogleNews-vectors-negative300.bin.gz"
echo "extracting vector file"
gunzip "GoogleNews-vectors-negative300.bin.gz"

echo "converting file to hdf5 for usage in the classifier"

../../../src/resources/Embeddings.py "GoogleNews-vectors-negative300.bin" "embeddings.hdf5"

echo "free space, remove original binary files"
rm "GoogleNews-vector-negative300.bin.gz"
rm "GoogleNews-vector-negative300.bin.gz"
