#!/bin/bash

supported_langs=("en" "it" "de" "es")

for lang in "${supported_langs[@]}"; do

	echo "downloading $lang fasttext embeddings"
	wget "https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.$lang.300.bin.gz"
	
	echo "extracting binary file"
	gunzip "cc.$lang.300.bin.gz"

	if [[ $lang -eq "en" ]]
	then
		desired_name='crawl'
	else
		desired_name=$lang
	fi;
	
	echo "rename the $lang bin file to fasttext-$desired_name.bin"
	mv "cc.$lang.300.bin" "fasttext-$desired_name.bin"
done

#wget https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.en.300.bin.gz

#tar xv cc.en.300.bin.gz
#rm cc.en.300.bin.gz

#mv cc.en.300.bin fastext-crawl.bin

#wget https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.it.300.bin.gz
#tar xv cc.it.300.bin.gz
#rm cc.it.300.bin.gz

#mv cc.it.300.bin fasttext-it.bin

#wget https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.de.300.bin.gz

#tar xv cc.de.300.bin.gz
#rm cc.de.300.bin.gz

#mv cc.de.300.bin fasttext-de.bin

#wget https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.es.300.bin.gz

#tar xv
