## Datasets


The classifier supports several datasets, that are not included in this repository.

* [hatespeech-waseem](https://github.com/zeerakw/hatespeech)

* [bullyingv3](http://research.cs.wisc.edu/bullying/data/bullyingV3.0.zip) 

* [instagram-cyberbullying (avaliable upon request)](https://arxiv.org/abs/1503.03909)

* [evalita hatespeech](http://www.di.unito.it/~tutreeb/haspeede-evalita18/data.html) (twitter and facebook)

* [germeval 2018 and 2019](https://projects.fzai.h-da.de/iggsa/projekt/) (avaliable upon registration for the shared tasks)

* [evalita misogyny](https://amievalita2018.wordpress.com/data/)

The datasets have to be copied to the appropriate directories in ``data``. Eg for evalita hatespede that would be ``data/evalita18/hatespeech``.
The hatespeech-waseem and bullyingv3 contain only tweet ids and they can be downloaded using the scripts in their respective folders.

## Embeddings

The system supports some pretrained embeddings that are available online:

* [google](https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit?usp=sharing)

* [twitter](http://yuca.test.iminds.be:8900/fgodin/downloads/word2vec_twitter_model.tar.gz)

* [fasttext](https://fasttext.cc/docs/en/crawl-vectors.html)

Some additional embeddings are not immediately available but can be replaced with any textual format embedding (in the correct language).

* italian

* german

Some multilingual embeddings were tested and are not available but can be replaced by properly aligned multilingual embeddings.


* multilingual-pivot: 3 languages (ita, ger, eng+ emoji embeddings)

* multilingual: Italian and English

* multilingual-ger: German and English

Download and conversion instructions are provided in the [embeddings readme](embeddings/Readme.md).
